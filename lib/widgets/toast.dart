import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Toast{
  static void toastMessage( String message) async {
    Fluttertoast.showToast(msg: message, backgroundColor: Colors.red, textColor: Colors.white,
        gravity: ToastGravity.BOTTOM,

    );
  }

}
