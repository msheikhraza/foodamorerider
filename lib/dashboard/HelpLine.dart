import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/OrderIssues.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

class topics {
  var image;
  var title;
  var isselected;

  topics({this.image, this.title, this.isselected});
}

class HelpLine extends StatefulWidget {
  const HelpLine({Key? key}) : super(key: key);

  @override
  _HelpLineState createState() => _HelpLineState();
}

class BottomDialog {
  void showBottomDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "showGeneralDialog",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.6),
      transitionDuration: const Duration(milliseconds: 400),
      context: context,
      pageBuilder: (context, _, __) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: _buildDialogContent(context),
        );
      },
      transitionBuilder: (_, animation1, __, child) {
        return SlideTransition(
          position: Tween(
            begin: const Offset(0, 1),
            end: const Offset(0, 0),
          ).animate(animation1),
          child: child,
        );
      },
    );
  }

  Widget _buildDialogContent(BuildContext context) {
    return IntrinsicHeight(
      child: Container(
        width: double.maxFinite,
        clipBehavior: Clip.antiAlias,
        padding: const EdgeInsets.only(top: 16),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        child: Material(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  TextWidget("Choose Order", false, FontWeight.bold, 1.6,
                      Colors.black, TextAlign.center, 'pnregular'),
                  Spacer(),
                  Image.asset(
                    'assets/cross.png',
                    height: 20,
                    width: 20,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                height: 240,
                color: Constants.whitesixteen,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    TextWidget(
                        "Choose the order that you have an issue with",
                        false,
                        FontWeight.w500,
                        1.3,
                        Colors.black,
                        TextAlign.start,
                        'pnregular'),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                        width: 360,
                        height: 190,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextWidget(
                                    "w0v-vllh (#1801)",
                                    false,
                                    FontWeight.w500,
                                    1.5,
                                    Constants.pinegreen,
                                    TextAlign.start,
                                    'AvenirDemi'),
                                SizedBox(
                                  height: 20,
                                ),
                                InkWell(
                                  child: Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 30,
                                        width: 30,
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          children: [
                                            Image.asset(
                                              'assets/shape.png',
                                              height: 20,
                                              width: 20,
                                            )
                                          ],
                                        ),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(10),
                                            color: Constants.faddedred),
                                      ),
                                      Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            TextWidget(
                                                "Collection",
                                                false,
                                                FontWeight.w500,
                                                1.5,
                                                Constants.pinegreen,
                                                TextAlign.start,
                                                'AvenirDemi'),
                                            TextWidget(
                                                "568 Beatty St, Vancouver,\n"
                                                    "BC V6B 2L3, Canada",
                                                false,
                                                FontWeight.w500,
                                                1.2,
                                                Colors.black.withOpacity(0.5),
                                                TextAlign.start,
                                                'pnregular'),
                                          ],
                                        ),
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: Image.asset(
                                          'assets/pathforw.png',
                                          height: 20,
                                          width: 20,
                                        ),
                                      ),
                                    ],
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                order_issues()));
                                  },
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 30,
                                      width: 30,
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/hacker.png',
                                            height: 20,
                                            width: 20,
                                          )
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(10),
                                          color: Constants.faddedred),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10, top: 5),
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          TextWidget(
                                              "Kent Farington",
                                              false,
                                              FontWeight.w500,
                                              1.2,
                                              Constants.pinegreen,
                                              TextAlign.start,
                                              'AvenirDemi'),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                              Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImage() {
    const image =
        'https://user-images.githubusercontent.com/47568606/134579553-da578a80-b842-4ab9-ab0b-41f945fbc2a7.png';
    return SizedBox(
      height: 88,
      child: Image.network(image, fit: BoxFit.cover),
    );
  }

  Widget _buildContinueText() {
    return const Text(
      'Continue with account',
      style: TextStyle(
        fontSize: 22,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget _buildEmapleText() {
    return const Text(
      'example.com',
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget _buildTextField() {
    const iconSize = 40.0;
    return Container(
      height: 60,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 1, color: Colors.grey.withOpacity(0.4)),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Row(
        children: [
          Container(
            width: iconSize,
            height: iconSize,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.grey[200],
            ),
            child: const Center(
              child: Text('Е'),
            ),
          ),
          const SizedBox(height: 16),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'elisa.g.beckett@gmail.com',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text('**********'),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildContinueButton() {
    return Container(
      height: 40,
      width: double.maxFinite,
      decoration: const BoxDecoration(
        color: Color(0xFF3375e0),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: RawMaterialButton(
        onPressed: () {
          // Navigator.of(context, rootNavigator: true).pop();
        },
        child: const Center(
          child: Text(
            'Continue',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}

class _HelpLineState extends State<HelpLine> {
  List<topics> topicslist = [
    topics(image: "assets/coffeeTime.png", title: "Break", isselected: false),
    topics(
        image: "assets/donation.png",
        title: "Pickup Issues",
        isselected: false),
    topics(image: "assets/bug.png", title: "App Issues", isselected: false),
    topics(
        image: "assets/coffeeTime.png",
        title: "Equipment Issues",
        isselected: false),
    topics(
        image: "assets/workingTime.png",
        title: "Shift issue",
        isselected: false),
    topics(image: "assets/cancel.png", title: "Order Issue", isselected: false),
    topics(image: "assets/accident.png", title: "Accident", isselected: false)
  ];

  BottomDialog bottomDialog = BottomDialog();

  int index = 1;
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    print(height.toString() + width.toString());
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldState,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 20*height,
                      width: 128.2*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 40,
                              ),
                              Spacer(),
                              TextWidget("Help", false, FontWeight.w500, 1.8,
                                  Colors.white, TextAlign.center, 'pnregular'),
                              Spacer(),
                              SizedBox(
                                width: 10,
                              ),
                              Image.asset(
                                'assets/cross.png',
                                height: 20,
                                color: Colors.white,
                                width: 20,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(top: 20, left: 15),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Column(
                        children: [
                          TextWidget("Select Topic!", false, FontWeight.w500,
                              1.9, Colors.black, TextAlign.left, 'pnregular'),
                        ],
                      ),
                    )),
                Expanded(
                  child: GridView.count(
                    crossAxisCount: 2,
                    // crossAxisSpacing: 10.0,
                    // mainAxisSpacing: 10.0,
                    childAspectRatio: 4 / 4,
                    shrinkWrap: true,
                    children: List.generate(
                      topicslist.length,
                          (index) {
                        return Container(
                          child: Card(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            semanticContainer: true,
                            color: Constants.whitetwo,
                            child: InkWell(
                              child: Container(
                                child: Stack(
                                  children: <Widget>[
                                    Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(bottom: 30),
                                        child: Image.asset(
                                          topicslist[index].image,
                                          height: 70,
                                          width: 70,
                                          color: topicslist[index].isselected
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(bottom: 10),
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.end,
                                          children: [
                                            Text(
                                                topicslist[index]
                                                    .title
                                                    .toString(),
                                                style: TextStyle(
                                                  color: topicslist[index]
                                                      .isselected
                                                      ? Colors.white
                                                      : Colors.black,
                                                  fontSize: 20.0,
                                                  fontWeight: FontWeight.bold,
                                                ))
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  color: !topicslist[index].isselected
                                      ? Colors.white
                                      : Constants.cobalt_two,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  if (topicslist[index].isselected) {
                                    topicslist[index].isselected = true;
                                  } else {
                                    topicslist[index].isselected = false;
                                  }

                                  if (topicslist[index].isselected) {
                                    topicslist[index].isselected = false;
                                  } else {
                                    topicslist[index].isselected = true;
                                  }
                                });
                              },
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.only(
                                top: 45, bottom: 45, left: 10, right: 10),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Container(
                  height: 110,
                  width: double.infinity,
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          width: 89.7 * width,
                          height: 10.24 * height,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: InkWell(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      TextWidget(
                                          "View Messages",
                                          false,
                                          FontWeight.w500,
                                          1.2,
                                          Constants.cobalt_two,
                                          TextAlign.center,
                                          'pnregular'),
                                      TextWidget(
                                          "Unread Messages",
                                          false,
                                          FontWeight.w500,
                                          0.8,
                                          Constants.pinegreen,
                                          TextAlign.start,
                                          'pnregular'),
                                    ],
                                  ),
                                  Spacer(),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 40,
                                    width: 40,
                                    child: Stack(
                                      children: [
                                        Align(
                                          child: Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                'assets/chat.png',
                                                height: 20,
                                                width: 20,
                                              )
                                            ],
                                          ),
                                          alignment: Alignment.center,
                                        ),
                                        Align(
                                          alignment: Alignment.topRight,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.black,
                                            radius: 8,
                                            child: TextWidget(
                                                "1",
                                                false,
                                                FontWeight.w100,
                                                0.5,
                                                Colors.white,
                                                TextAlign.center,
                                                'AvenirBold'),
                                          ),
                                        )
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(200),
                                        color: Constants.faddedred),
                                  ),
                                ],
                              ),
                              onTap: () {
                                bottomDialog.showBottomDialog(context);
                              },
                            ),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
