import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/NewOrder.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/bottom_nav.dart';
import 'package:firebase_database/firebase_database.dart';

class deliveriesScreen extends StatefulWidget {
  const deliveriesScreen({Key? key}) : super(key: key);

  @override
  _deliveriesScreenState createState() => _deliveriesScreenState();
}

class _deliveriesScreenState extends State<deliveriesScreen> {

  final databaseReference = FirebaseDatabase.instance.reference();
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery
        .of(context)
        .size
        .height / 100;
    var width = MediaQuery
        .of(context)
        .size
        .width / 100;
    return Container(
        color: Constants.faddedred,
        child: SafeArea(
            child: Scaffold(
              key: _scaffoldState,
              drawer: DrawerScreen(),
              backgroundColor: Constants.whitetwo,

                body: Container(
                  child: Column(

                    children: [

                      Stack(
                        children: [
                          Container(
                            height: 10.2*height,
                            width: 125*width,
                            decoration: BoxDecoration(
                                color: Constants.faddedred,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 20, left: 15),
                            child: Column(
                              children: [
                                Row(
                                  children: [

                                    InkWell(

                                      child: Image.asset(
                                        'assets/drawer.png',
                                        height: 5.4*height,
                                        width: 5.4*width,
                                      ),
                                      onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                    ),
                                    Spacer(),
                                    InkWell(
                                      child: TextWidget(
                                          "Deliveries",
                                          false,
                                          FontWeight.w500,
                                          1.5,
                                          Colors.white,
                                          TextAlign.center,
                                          'pnregular'),
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (BuildContext context) => bottomnav(index: 1,check: 4,)));
                                      },



                                    ),
                                    Spacer(),
                                    Image.asset(
                                      'assets/nounSupport.png',
                                      height: 5.4*height,
                                      width: 5.4*width,
                                    ),
                                    SizedBox(
                                      width: 2.5*width,
                                    ),
                                    SizedBox(
                                      width: 3.75*width,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 38.4*height),

                      Center(
                        child: TextWidget(
                            "You have no pending Deliveries",
                            false,
                            FontWeight.w400,
                            1.2,
                            Colors.black,
                            TextAlign.center,
                            'AvenirDemi'),
                      ),
                      SizedBox(height: 5,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: TextWidget(
                            "Please make sure to move towards the center of your zone",
                            false,
                            FontWeight.w500,
                            1.1,
                            Colors.blueGrey,
                            TextAlign.center,
                            'AvenirDemi'),
                      )


                    ],
                  ),
                ),
        ),
      ),
    );
  }
}


//   void createData() {
//     databaseReference.child("flutterDevsTeam1").set({
//       'name': 'Deepak Nishad',
//       'description': 'Team Lead'
//     });
//     databaseReference.child("flutterDevsTeam2").set({
//       'name': 'Yashwant Kumar',
//       'description': 'Senior Software Engineer'
//     });
//     databaseReference.child("flutterDevsTeam3").set({
//       'name': 'Akshay',
//       'description': 'Software Engineer'
//     });
//     databaseReference.child("flutterDevsTeam4").set({
//       'name': 'Aditya',
//       'description': 'Software Engineer'
//     });
//     databaseReference.child("flutterDevsTeam5").set({
//       'name': 'Shaiq',
//       'description': 'Associate Software Engineer'
//     });
//     databaseReference.child("flutterDevsTeam6").set({
//       'name': 'Mohit',
//       'description': 'Associate Software Engineer'
//     });
//     databaseReference.child("flutterDevsTeam7").set({
//       'name': 'Naveen',
//       'description': 'Associate Software Engineer'
//     });
//   }
//
//   void readData() {
//     databaseReference.once().then((snapshot)  => {
//       print('Data : ${snapshot.snapshot.value}'),
//     });
//   }
//
//   void updateData() {
//     databaseReference.child('flutterDevsTeam1').update({
//       'description': 'CEO'
//     });
//     databaseReference.child('flutterDevsTeam2').update({
//       'description': 'Team Lead'
//     });
//     databaseReference.child('flutterDevsTeam3').update({
//       'description': 'Senior Software Engineer'
//     });
//   }
//
//   void deleteData() {
//     databaseReference.child('flutterDevsTeam4').remove();
//     databaseReference.child('flutterDevsTeam5').remove();
//     databaseReference.child('flutterDevsTeam6').remove();
//     databaseReference.child('flutterDevsTeam7').remove();
//   }
// }