import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/Models/GetRideData.dart';
import 'package:foodamorerider/dashboard/OrderDialog.dart';
import 'package:foodamorerider/dashboard/availableShift.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodamorerider/components/DriverPopup.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;

  bool isInitialDataLoaded = false;
  @override
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  FirebaseDatabase db = FirebaseDatabase.instance;


// timer(){
//   Timer.periodic(Duration(seconds: 5), (timer) {
//     print(DateTime.now());
//     listenDataFromFireBase();
//   });
// }

  @override
  void initState() {
    // TODO: implement initState

    // timer();
    db.ref().child("riderrequests").onValue.listen((event) {

      print(event.snapshot.key);
      Map<dynamic, dynamic> values =
          event.snapshot.value as Map<dynamic, dynamic>;
      RideDatalist = [];

      values.forEach((key, value) {
        setState(() {
          RideDatalist.add(RideData(
            customerId: value['customerId'],
            customerLatitude: value['customerLatitude'],
            customerLongitude: value['customerLongitude'],
            customerName: value['customerName'],
            orderAddress: value['orderAddress'],
            orderId: value['orderId'],
            orderNote: value['orderNote'],
            orderDetails: value['orderDetails'],
            shopName: value['shopName'],
            status: value['status'],
            totalPrice: value['totalPrice'],
            vendorId: value['vendorId'],
            vendorLatitude: value['vendorLatitude'],
            vendorLongitude: value['vendorLongitude'],
            vendorName: value['vendorName'],
          ));
        });

      });
      String status;
db.ref().child("riderrequests").child(RideDatalist[0].orderId.toString()).once().then((snapshot)
{
  Map<dynamic, dynamic> values = snapshot.snapshot.value as Map<dynamic, dynamic>;
  print(values["status"]);
  status = values["status"];
  print(status + "Status   ");
  if(status == "Accept"){

  } else{

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return OrderDialogBox();
        });
  }

});
      setState(() {
        isInitialDataLoaded = true;
print(values['b8e929bddd344a8e9bec']['status'].toString());



        // showAlertDialog(context);

        print("GET_NEW_DATA");
      });
    });
    super.initState();
  }



  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    // print(height.toString()+ width.toString());

    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldState,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 41 * height,
                        width: 128 * width,
                        child: Image.asset('assets/designClipart.png'),
                        decoration: BoxDecoration(
                            color: Constants.faddedred,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(60),
                              bottomRight: Radius.circular(60),
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20, left: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: () =>
                                      _scaffoldState.currentState!.openDrawer(),
                                  child: Image.asset(
                                    'assets/drawer.png',
                                    height: 4.5 * height,
                                    width: 4.5 * width,
                                  ),
                                ),
                                Spacer(),
                                Padding(
                                  padding: const EdgeInsets.only(right: 140),
                                  child: TextWidget(
                                      "Status",
                                      false,
                                      FontWeight.w500,
                                      1.8,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),
                                ),
                                SizedBox(
                                  width: 0.2 * width,
                                ),
                                SizedBox(
                                  width: 3.7 * width,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 0.15 * height,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, right: 22, left: 10),
                              child: Container(
                                margin: EdgeInsets.only(left: 0),
                                height: 7.6 * height,
                                width: 100 * width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 10, top: 8),
                                      width: 35 * width,
                                      height: 5.7 * height,
                                      decoration: BoxDecoration(
                                          color: Constants.faddedred
                                              .withOpacity(0.2),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Center(
                                        child: TextWidget(
                                            "Not Working",
                                            false,
                                            FontWeight.w700,
                                            1.0,
                                            Colors.black,
                                            TextAlign.center,
                                            'AvenirNextRegular'),
                                      ),
                                    ),
                                    Spacer(),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 2.5 * height,
                            ),
                            Divider(),
                            Container(
                              margin: EdgeInsets.only(right: 16),
                              height: 42 * height,
                              width: 87.5 * width,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 30.0, top: 20),
                                    child: TextWidget(
                                        "Search Shifts",
                                        false,
                                        FontWeight.w800,
                                        1.5,
                                        Colors.black,
                                        TextAlign.center,
                                        'AvenirBold'),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 30.0, top: 5),
                                    child: TextWidget(
                                        "Oh, you can’t work without shifts!",
                                        false,
                                        FontWeight.w400,
                                        1.1,
                                        Constants.warmgrey,
                                        TextAlign.center,
                                        'AvenirNextRegularr'),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 50, top: 0),
                                    child: Image.asset(
                                      'assets/ride.png',
                                      height: 29.4 * height,
                                      width: 62.5 * width,
                                    ),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white,
                                //
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: Colors.grey.withOpacity(0.5),
                                //     spreadRadius: 5,
                                //     blurRadius: 7,
                                //     offset: Offset(0, 3), // changes position of shadow
                                //   ),
                                // ],
                              ),
                            ),
                            SizedBox(
                              height: 2.5 * height,
                            ),
                            InkWell(
                              child: Container(
                                margin: EdgeInsets.only(right: 16),
                                width: MediaQuery.of(context).size.width / 1.1,
                                height: 7.4 * height,
                                child: Center(
                                  child: TextWidget(
                                      "Search Shifts",
                                      false,
                                      FontWeight.w500,
                                      1.2,
                                      Colors.white,
                                      TextAlign.center,
                                      'AvenirDemi'),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(11),
                                  color: Constants.faddedred.withOpacity(0.9),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        avShifts()));
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
