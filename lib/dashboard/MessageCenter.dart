import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/History.dart';
import 'package:foodamorerider/dashboard/Maps.dart';

class MessageCenter extends StatefulWidget {
  const MessageCenter({Key? key}) : super(key: key);

  @override
  _MessageCenterState createState() => _MessageCenterState();
}

class _MessageCenterState extends State<MessageCenter> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 10.25*height,
                      width: 125*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              History()));
                                },
                                child: Image.asset(
                                  'assets/iconArrowBack.png',
                                    height: 5.4*height,
                                    width: 5.4*width
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: InkWell(
                                  child: InkWell(
                                    child: TextWidget(
                                        "Message Center",
                                        false,
                                        FontWeight.w500,
                                        1.6,
                                        Colors.white,
                                        TextAlign.center,
                                        'pnregular'),
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                History()));
                                  },
                                ),
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                  height: 5.4*height,
                                  width: 5.4*width
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                  ],
                ),
                Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20, left: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              height: 40,
                              width: 40,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/vespa.png',
                                    height: 30,
                                    width: 35,
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(200),
                                  color: Constants.faddedred),
                            ),
                            Container(
                              width: 62.5*width,
                              height: 100,
                              margin: EdgeInsets.only(left: 20),
                              decoration: BoxDecoration(
                                  color: Constants.whiteFifteen,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(20),
                                    topLeft: Radius.circular(20),
                                    bottomLeft: Radius.circular(20),
                                    bottomRight: Radius.circular(20),
                                  )),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    TextWidget(
                                        "I’m on my way.",
                                        false,
                                        FontWeight.w500,
                                        1.1,
                                        Constants.pinegreen,
                                        TextAlign.start,
                                        'pnregular'),
                                    TextWidget(
                                        "Do you need any further assistance?",
                                        false,
                                        FontWeight.w500,
                                        1.1,
                                        Constants.pinegreen,
                                        TextAlign.start,
                                        'pnregular')

                                  ],
                                ),
                              ),
                            ),


                          ],
                        ),
                      ),
                    ),

                  ],
                ),

                Flexible(
                  flex: 1,
                  child: Card(
                    elevation: 5,
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight:Radius.circular(10),  )),
                      margin: EdgeInsets.only(top: 30),
                      height: 60,
                      child: Row(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                        Container(child: Image.asset("assets/gallery.png"),)
                        ,Container(
                            height: 20,
                          width: 250,
                          child: TextFormField(

                           decoration: InputDecoration(labelText:"Type your question here...",

                             border: InputBorder.none,
                             focusedBorder: InputBorder.none,
                             enabledBorder: InputBorder.none,
                             errorBorder: InputBorder.none,
                             disabledBorder: InputBorder.none,
                           ),


                          ),


                        ),
                          TextWidget(
                              "Send",
                              false,
                              FontWeight.w500,
                              1.3,
                              Constants.rosepink,
                              TextAlign.start,
                              'pnregular')

                      ],),

                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
