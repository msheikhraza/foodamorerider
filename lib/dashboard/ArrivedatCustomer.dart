import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/Models/Chat/chatfirebasescreen.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/DropOff.dart';
import 'package:foodamorerider/dashboard/DropOff.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'package:geocoding/geocoding.dart' as geo;
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;
import 'package:stop_watch_timer/stop_watch_timer.dart';

class GotoCostumer extends StatefulWidget {
  const GotoCostumer({Key? key}) : super(key: key);

  @override
  _GotoCostumerState createState() => _GotoCostumerState();
}

class _GotoCostumerState extends State<GotoCostumer> {
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
    Completer<GoogleMapController> _controller = Completer();
var secdata;
var mindata;
String point ="";
String distance = "";
  bool checkPlatform = Platform.isIOS;
  LatLng? fromLocation;
  LatLng? tolocation;
  GoogleMapController? controller;
  bool check = false;
  Timer? timer;
String customerAddress ="";
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  final Set<Polyline> polyline = {};
  @override
  void initState() {
    // TODO: implement initState
    final _stopWatchTimer = StopWatchTimer(
     mode: StopWatchMode.countUp,
      onChange: (value) {
        final displayTime = StopWatchTimer.getDisplayTime(value);
      },
      onChangeRawSecond: (value){
        setState(() {
          secdata = value;
        });
      },
      onChangeRawMinute: (value) {
        mindata = value;
    }
    );
    _stopWatchTimer.clearPresetTime();
    _stopWatchTimer.onExecute.add(StopWatchExecute.start);
    customerLatLong(double.parse(RideDatalist[0].customerLatitude), double.parse(RideDatalist[0].customerLongitude));
    tolocation = LatLng(double.parse(RideDatalist[0].customerLatitude), double.parse(RideDatalist[0].customerLongitude));
    if(check == true){
      timer?.cancel();
    }else{
      timer = Timer.periodic(Duration(seconds: 5), (Timer t) =>   getCurrentLocation());
    }
    initmarkers();

    super.initState();
  }
  Future<void> customerLatLong(lattitude, longitude) async {
    print(lattitude.toString() + " " + " " + longitude.toString());

    List<geo.Placemark> placemarks =
    await geo.placemarkFromCoordinates(lattitude,longitude);
    print(placemarks);
    geo.Placemark place = placemarks[0];
    String address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {
     customerAddress = address;
    });
  }

 final markersget = Set<Marker>();
   buildGoogleMap() {
    return Stack(
      children: [
        GoogleMap(
          
          polylines: polyline,
          myLocationButtonEnabled: true,
          zoomControlsEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            setState(() {
              _controller.complete(controller);
            });
          },
          onCameraMoveStarted: () {
            setState(() {
              controller?.moveCamera(CameraUpdate.zoomBy(0));
            });
          },
          onCameraMove: (position) {
            setState(() {
              controller?.moveCamera(CameraUpdate.zoomBy(0));
              markersget.add(Marker(
                  markerId: MarkerId("My Location"),
                  position: position.target));
            });
          },
          markers: Set<Marker>.of(markers.values),
          padding: EdgeInsets.only(bottom: 300, top: 250),
      
          initialCameraPosition: CameraPosition(
            target: LatLng(lat,long) ,
            zoom: 11.0,
          ),
          onTap: (position) {},
        ),
     
       Align(
            alignment: Alignment.bottomRight,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                    onPressed: () async {
                      var location = await _locationTracker.getLocation();
                      final GoogleMapController mapController =
                          await _controller.future;
                      mapController.animateCamera(CameraUpdate.newLatLngZoom(
                          LatLng(
                              double.parse(
                                location.latitude.toString(),
                              ),
                              double.parse(location.longitude.toString())),
                          11.0));

                      if (check == true) {
                      } else {
                        // updatefirebase(location);
                      }
                    },
                    icon: Icon(
                      Icons.my_location,
                    )))),
      ],
    );
  }



  BitmapDescriptor? CarIcon;
  Future initmarkers() async{
    CarIcon = await getBitmapDescriptorFromAssetBytes('assets/delivery-man.png', 60);
  }
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }
  Future<BitmapDescriptor> getBitmapDescriptorFromAssetBytes(
      String path, int width) async {
    final Uint8List imageData = await getBytesFromAsset(path, width);
    return BitmapDescriptor.fromBytes(imageData);
  }
  Location _locationTracker = Location();
  StreamSubscription? _locationSubscription;
  Future getCurrentLocation() async {
    try {

     final GoogleMapController mapController = await _controller.future;

      var location = await _locationTracker.getLocation();
      fromLocation = LatLng(double.parse(location.latitude.toString()), (double.parse(location.longitude.toString())));

      updateMarkerAndCircle(location);
      if(check == true){

      } else{
        updatefirebase(location);
      }

      if (_locationSubscription != null) {
        _locationSubscription?.cancel();
      }
      _locationSubscription =
          _locationTracker.onLocationChanged.listen((newLocalData) {
          
              mapController.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(
                    target: LatLng(double.parse(newLocalData.latitude.toString()), double.parse(newLocalData.longitude.toString())),
                    zoom: 16,
                    bearing: double.parse(newLocalData.heading.toString()),
                  ),
                ),
              );
              fromLocation =  LatLng(double.parse(newLocalData.latitude.toString()), double.parse(newLocalData.longitude.toString()));
              updateMarkerAndCircle(newLocalData);
              if(check == true){

              } else{
                updatefirebase(location);
              }
            


          });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }
  void updateMarkerAndCircle(LocationData newLocalData)async {
    final GoogleMapController mapController = await _controller.future;
    checkPlatform ? print('ios') : print("android");
    final MarkerId markerIdFrom = MarkerId("My Location");

    final Marker marker = Marker(
      markerId: markerIdFrom,
      //position: LatLng(_fromLocation.latitude, _fromLocation.longitude),
      position: LatLng(double.parse(newLocalData.latitude.toString()), double.parse(newLocalData.longitude.toString())),
      infoWindow: InfoWindow(title: "Current"),
      icon: checkPlatform ? CarIcon! : CarIcon!,
    );
    if (mounted) {
      setState(() {
        markers[markerIdFrom] = marker;
      });
    } 
  
      mapController.animateCamera(CameraUpdate.newCameraPosition(
          new CameraPosition(
              bearing: double.parse(newLocalData.heading.toString()),
              target: LatLng(double.parse(newLocalData.latitude.toString()), double.parse(newLocalData.longitude.toString())),
              tilt: 0,
              zoom: 16.00)));

  }


  setPolylines(LatLng a, LatLng b ) async {
    String url =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${a.latitude},${a.longitude}&destination=${RideDatalist[0].customerLatitude},${RideDatalist[0].customerLongitude}&key=$googleAPIKey";
    http.Response response = await http.get(Uri.parse(url));
    Map values = jsonDecode(response.body);
    point = values["routes"][0]["overview_polyline"]["points"];
    distance = values["routes"][0]["legs"][0]["distance"]["text"];

    // create a Polyline instance
    // with an id, an RGB color and the list of LatLng pairs
setState(() {
  polyline.add(Polyline(
      polylineId: PolylineId('route1'),
      visible: true,
      points: convertToLatLng(decodePoly(point)),
      width: 2,
      color: Colors.red,
      startCap: Cap.roundCap,
      endCap: Cap.buttCap));
});

    addmarker(LatLng(double.parse(RideDatalist[0].customerLatitude), double.parse(RideDatalist[0].customerLongitude)));

    return values["routes"][0]["overview_polyline"]["points"];
  }
  void addmarker(var a) {
    checkPlatform ? print('ios') : print("android");
    final MarkerId markerIdFrom = MarkerId("CustomerId");
    final Marker marker = Marker(
      markerId: markerIdFrom,
      //position: LatLng(_fromLocation.latitude, _fromLocation.longitude),
      position: LatLng(a.latitude, a.longitude),

      infoWindow: InfoWindow(title: "Destination"),
      icon: checkPlatform
      // ? BitmapDescriptor.fromAsset("assets/car_icon.png")
      // : BitmapDescriptor.fromAsset("assets/car_icon.png"),
          ? BitmapDescriptor.defaultMarker
          : BitmapDescriptor.defaultMarker,
    );
    if (mounted) {
      setState(() {
        markers[markerIdFrom] = marker;
      });
    }
  }
  static List decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = [];
    int index = 0;
    int len = poly.length;
    int c = 0;
    // repeating until all attributes are decoded
    do {
      var shift = 0;
      int result = 0;

      // for decoding value of one attribute
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      /* if value is negative then bitwise not the value */
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    /*adding to previous value as done in encoding */
    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    print(lList.toString());

    return lList;
  }
  static List<LatLng> convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }
  FirebaseAuth _auth = FirebaseAuth.instance;
  final databaseReference = FirebaseDatabase.instance.ref();
  updatefirebase(var newLocalData) {

    if (_auth.currentUser!.uid.isNotEmpty) {
      databaseReference.child("ridertocustomer").child(_auth.currentUser!.uid).update({
        'riderlat': newLocalData.latitude,
        'riderlong': newLocalData.longitude,
        "customerid": RideDatalist[0].customerId,
        "vendorid": RideDatalist[0].vendorId,
  "riderid": FirebaseAuth.instance.currentUser!.uid,

      }).then((value) {
        setPolylines(fromLocation!, tolocation!);
      });

      if (lat != null) {
        setPolylines(fromLocation!, tolocation!);
      }
    }

  }






























  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(

          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 10.25*height,
                      width: 125*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                child: Image.asset(
                                  'assets/drawer.png',
                                    height: 5.4*height,
                                    width: 5.4*width
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: InkWell(
                                  child: TextWidget(
                                      "Go to Customer",
                                      false,
                                      FontWeight.w500,
                                      1.6,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),

                                  // onTap: (){
                                  //   Navigator.of(context).push(MaterialPageRoute(
                                  //       builder: (BuildContext context) => VendorsScreen()));
                                  // },
                                ),

                                // onTap: (){
                                //   Navigator.of(context).push(MaterialPageRoute(
                                //       builder: (BuildContext context) => newOrder()));
                                // },
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                  height: 5.4*height,
                                  width: 5.4*width
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.8 * height,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2.4 * height,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextWidget("Costumer Details", false, FontWeight.w100,
                            1.2, Colors.black, TextAlign.center, 'AvenirDemi'),

                      ],
                    ),
                  ),
                ),
                SizedBox(height: 2.4 * height,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: TextWidget(
                           RideDatalist[0].customerName,
                              false,
                              FontWeight.w100,
                              1.4,
                              Colors.black,
                              TextAlign.center,
                              'AvenirBold'),
                        ),
                        Spacer(),
                        Container(
                          height: 40,
                          width: 40,
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: Image.asset(
                              'assets/locate.png',
                              fit: BoxFit.fitHeight,

                            ),
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),

                              color: Constants.faddedred),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 40,
                          width: 40,
                          child: Stack(
                            children: [
                              Align(
                                child: InkWell(
                                  onTap: (){
                                    print(FirebaseAuth.instance.currentUser!.uid.toString()+"<-------->"+RideDatalist[0].customerId);
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (BuildContext context) => ChatScreen(FirebaseAuth.instance.currentUser!.uid.toString(),RideDatalist[0].customerId)));
                                  },
                                  child: Image.asset(
                                    'assets/chat.png',
                                    height: 20,
                                    width: 20,
                                  ),
                                ),
                                alignment: Alignment.center,
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: CircleAvatar(
                                  backgroundColor: Colors.black,
                                  radius: 8,
                                  child: TextWidget(
                                      "1",
                                      false,
                                      FontWeight.w100,
                                      0.5,
                                      Colors.white,
                                      TextAlign.center,
                                      'AvenirBold'),
                                ),
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              color: Constants.faddedred),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 40,
                          width: 40,
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            'assets/call.png',
                            height: 20,
                            width: 20,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(200),
                              color: Constants.faddedred),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    //here it is
                    height: 50,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 25,
                          width: 25,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/pin.png',
                                height: 15,
                                width: 15,
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Constants.faddedred),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextWidget("Location", false, FontWeight.w100, 1.2,
                                Colors.black, TextAlign.center, 'AvenirDemi'),
                            Container(
                              width: 280,
                              height: 20,


                              child: TextWidget(
                                  customerAddress,
                                  false,
                                  FontWeight.w100,
                                  1.0,
                                  Colors.black,
                                  TextAlign.start,
                                  'AvenirNextRegular'),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Divider(
                  thickness: 2,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    width: double.infinity,
                    height: 60,
                    //color: Colors.yellow,

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10 ),
                          width: 150,
                          height: double.infinity,
                          //color: Colors.yellow,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextWidget("Payment", false, FontWeight.w200, 1.2,
                                  Constants.pinegreen.withOpacity(0.5), TextAlign.start, 'AvenirDemi'),
                              SizedBox(
                                height: 5,
                              ),
                              TextWidget(
                                  "Paid Online",
                                  false,
                                  FontWeight.w500,
                                  1.2,
                                  Colors.black,
                                  TextAlign.start,
                                  'AvenirDemi')
                            ],
                          ),
                        ),
                        Spacer(),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: Column(
                            children: [
                              TextWidget(
                              "\$"+RideDatalist[0].totalPrice.toString(),
                                  false,
                                  FontWeight.w500,
                                  1.9,
                                  Constants.faddedred,
                                  TextAlign.start,
                                  'AvenirNextBold'),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Divider(
                  thickness: 2,
                ),
                Container(
                  child: Column(
                    children: [
                      TextWidget("Arrive at $mindata: $secdata", false, FontWeight.w100, 1.2,
                          Colors.black, TextAlign.center, 'AvenirBold')
                    ],
                  ),
                ),
                SizedBox(height: 10,),
                Center(
                  child: Container(
                    width: 80 * width,
                    height: 25.64 * height,
                    child: buildGoogleMap(),
                  ),
                ),
                SizedBox(height: 40,),
                InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    height: 58,
                    child: Center(
                      child: TextWidget(
                          "Arrived at the Customer",
                          false,
                          FontWeight.w500,
                          1.2,
                          Colors.white,
                          TextAlign.center,
                            'AvenirDemi'),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: Constants.faddedred),
                  ),
                   onTap: (){


                     databaseReference.child("ridertocustomer").child(_auth.currentUser!.uid).remove().then((value) {
                       check =true;

                       Navigator.of(context).push(MaterialPageRoute(
                           builder: (BuildContext context) => drop_off()));
                     });

                   },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
