import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/NoPendingDeliveries.dart';

class OnGoingScreen extends StatefulWidget {
  const OnGoingScreen({Key? key}) : super(key: key);

  @override
  _OnGoingScreenState createState() => _OnGoingScreenState();
}

class _OnGoingScreenState extends State<OnGoingScreen> {
  bool isSwitched = false;
  var textValue = 'Switch is OFF';

  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    } else {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    print(height.toString()+   width.toString());
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer:    DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 41.0*height,
                        width: 125*width,
                        child: Image.asset('assets/designClipart.png'),
                        decoration: BoxDecoration(
                            color: Constants.faddedred,
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60), bottomRight: Radius.circular(60),
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only( left: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                  child: Image.asset(
                                    'assets/drawer.png',
                                    height: 5.4*height,
                                    width: 5.4*width,
                                  ),
                                ),
                                Spacer(),
                                TextWidget(
                                    "Status",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                                Spacer(),
                                Image.asset(
                                  'assets/nounSupport.png',
                                  height: 5.4*height,
                                  width: 5.4*width,
                                ),
                                SizedBox(
                                  width: 2.5*width,
                                ),
                                SizedBox(
                                  width: 3.75*width,
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 20, right: 30),
                              child: Container(
                                height: 7.69*height,
                                width: 100*width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        width: 32.5*width,
                                        height: 6.41*height,
                                        decoration: BoxDecoration(
                                            color: Constants.faddedred.withOpacity(0.2),
                                            borderRadius:
                                            BorderRadius.circular(10)),
                                        child: Center(
                                          child: TextWidget(
                                              "Working",
                                              false,
                                              FontWeight.w400,
                                              1.1,
                                              Colors.black,
                                              TextAlign.center,
                                              'pnregular'),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.all(18.0),
                                      child: TextWidget(
                                          "Request Break",
                                          false,
                                          FontWeight.w500,
                                          1.1,
                                          Constants.faddedred,
                                          TextAlign.center,
                                          'AvenirBold'),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: InkWell(
                                  child: TextWidget(
                                      "Current Shifts",
                                      false,
                                      FontWeight.w500,
                                      1.3,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                deliveriesScreen()));
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 2.5*height,
                            ),
                            Divider(),
                            InkWell(
                              child: Container(

                                margin: EdgeInsets.only(right: 16),
                                height: 27.9*height,
                                width: 84.7*width,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10 , top: 10),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              height: 12.8*height,
                                              width: 20*width,
                                              decoration: BoxDecoration(
                                                color: Constants.faddedred.withOpacity(0.2),
                                                borderRadius:
                                                BorderRadius.circular(20),

                                              ),
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        top: 10.0),
                                                    child: TextWidget(
                                                        "Sep",
                                                        false,
                                                        FontWeight.w800,
                                                        1.0,
                                                        Constants.faddedred.withOpacity(0.7),
                                                        TextAlign.center,
                                                        'AvenirReg'),
                                                  ),
                                                  TextWidget(
                                                      "07",
                                                      false,
                                                      FontWeight.w500,
                                                      2.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'pnregular'),
                                                  TextWidget(
                                                      "Today",
                                                      false,
                                                      FontWeight.w300,
                                                      1.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'pnregular'),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 10),
//height
                                            height: 12.8*height,
                                            width: 50*width,
                                            //color: Colors.black,

                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    TextWidget(
                                                        "24:49  -  23:45",
                                                        false,
                                                        FontWeight.w500,
                                                        1.1,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                    TextWidget(
                                                        "(56mins)",
                                                        false,
                                                        FontWeight.w500,
                                                        1.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                  ],
                                                ),
                                                SizedBox(height: 4,),
                                                TextWidget(
                                                    "Vancouver",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.black,
                                                    TextAlign.center,
                                                    'pnregular'),
                                                SizedBox(height: 4,),
                                                Container(
                                                  height: 3.8*height,
                                                  width: 37.5*width,
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        Icons
                                                            .check_circle_outline,
                                                        size: 20,
                                                        color:
                                                        Constants.faddedred,
                                                      ),
                                                      SizedBox(height: 2,),
                                                      TextWidget(
                                                          "Ongoing",
                                                          false,
                                                          FontWeight.w300,
                                                          1.2,
                                                          Constants.faddedred,
                                                          TextAlign.center,
                                                          'pnregular'),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Divider(
                                          height: 15,
                                          thickness: 2,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 10 , right: 10),
                                        child: Row(
                                          children: [
                                            TextWidget(
                                                "Available for Shift Extension",
                                                false,
                                                FontWeight.w700,
                                                1,
                                                Colors.black,
                                                TextAlign.center,
                                                'pnregular'),
                                            Spacer(),
                                            Container(
                                              height: 30,
                                              width: 50,
                                              child: FlutterSwitch(
                                                value: isSwitched,
                                                activeColor: Constants.faddedred,
                                                inactiveColor: Constants.pigpink,
                                                onToggle: (val) {
                                                  setState(() {
                                                    isSwitched = val;
                                                  });
                                                },
                                              ),
                                            ),
                                            // CustomSwitch(
                                            //   value: isSwitched,
                                            //   activeColor: Colors.blue,
                                            //   onChanged: (value) {
                                            //     print("VALUE : $value");
                                            //     setState(() {
                                            //       isSwitched = value;
                                            //     });
                                            //   },
                                            // ),
                                          ],
                                        ),
                                      ),

                                    ],
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: Colors.white,

                                ),
                              ),
                              onTap: () {


                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}