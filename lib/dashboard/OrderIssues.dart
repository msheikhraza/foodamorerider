import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'package:expansion_widget/expansion_widget.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

class order_issues extends StatefulWidget {
  const order_issues({Key? key}) : super(key: key);

  @override
  _order_issuesState createState() => _order_issuesState();
}

const loremIpsum =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

class Card1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    print(height.toString() + width.toString());
    return ExpandableNotifier(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: <Widget>[
                ExpandablePanel(
                  header: Padding(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 30,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextWidget("1", false, FontWeight.w500, 1.5,
                                  Colors.white, TextAlign.start, 'AvenirDemi')
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Constants.faddedred),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: TextWidget(
                                    "Redispatch Order?",
                                    false,
                                    FontWeight.w500,
                                    1.5,
                                    Constants.pinegreen,
                                    TextAlign.start,
                                    'AvenirDemi'),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextWidget(
                                  "Order #82965865 pickup time was",
                                  false,
                                  FontWeight.w500,
                                  1.0,
                                  Colors.black,
                                  TextAlign.start,
                                  'pnregular'),
                              SizedBox(
                                height: 10,
                              ),
                              TextWidget(
                                  "11:45 PM",
                                  false,
                                  FontWeight.w500,
                                  1.2,
                                  Constants.pinegreen,
                                  TextAlign.start,
                                  'pnregular'),
                            ],
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  collapsed: Container(
                    width: double.infinity,
                    height: 25.6 * height,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          height: 19.23 * height,
                          width: 78.92 * width,
                          child: Column(
                            children: [
                              TextWidget(
                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation",
                                  false,
                                  FontWeight.w500,
                                  1.3,
                                  Constants.pinegreen,
                                  TextAlign.start,
                                  'pnregular')
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          width: 33.3 * width,
                          height: 5.41 * height,
                          child: Center(
                            child: TextWidget("Next", false, FontWeight.w500, 1.4,
                                Colors.white, TextAlign.center, 'AvenirDemi'),
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(11),
                              color: Constants.faddedred),
                        )
                      ],
                    ),
                  ),
                  expanded: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //Text Can be shown here
                    ],
                  ),
                  builder: (_, collapsed, expanded) {
                    return Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                      child: Expandable(
                        collapsed: collapsed,
                        expanded: expanded,
                        theme: const ExpandableThemeData(crossFadePoint: 0),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ));
  }
}

class Card2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    print(height.toString() + width.toString());
    return ExpandableNotifier(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: <Widget>[
                ExpandablePanel(
                  header: Padding(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 30,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextWidget("1", false, FontWeight.w500, 1.5,
                                  Colors.white, TextAlign.start, 'AvenirDemi')
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Constants.faddedred),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              TextWidget(
                                  "Redispatch the Order?",
                                  false,
                                  FontWeight.w500,
                                  1.5,
                                  Constants.pinegreen,
                                  TextAlign.start,
                                  'AvenirDemi'),
                              SizedBox(
                                height: 10,
                              ),
                              TextWidget(
                                  "You can redipatch the order to \nanother Rider",
                                  false,
                                  FontWeight.w500,
                                  1.2,
                                  Colors.black,
                                  TextAlign.start,
                                  'pnregular'),
                            ],
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  collapsed: Container(
                    width: double.infinity,
                    height: 10.53 * height,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          height: 10.53 * height,
                          width: 76.92 * width,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                width: 300,
                                height: 50,
                                child: Center(
                                  child: TextWidget(
                                      "Redipatch the order",
                                      false,
                                      FontWeight.w500,
                                      1.4,
                                      Colors.white,
                                      TextAlign.center,
                                      'AvenirDemi'),
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(11),
                                    color: Constants.cobalt_two),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  expanded: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //Text Can be shown here
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class _order_issuesState extends State<order_issues> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    print(height.toString());
    print(width.toString());

    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 10.25 * height,
                      width: 128.2 * width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/iconArrowBack.png',
                            height: 25,
                            width: 25,
                          ),
                          Spacer(),
                          TextWidget(
                              "Order Issues",
                              false,
                              FontWeight.w500,
                              1.8,
                              Colors.white,
                              TextAlign.center,
                              'pnregular'),
                          Spacer(),
                          Image.asset(
                            'assets/cross.png',
                            height: 20,
                            width: 20,
                            color: Colors.white,
                          ),SizedBox(width: 10,),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.56 * height,
                ),
                SingleChildScrollView(
                  child: Container(
                    height: 51.28 * height,
                    width: 102.5 * width,
                    // color: Constants.cobalt_two,
                    child: ListView(
                      physics: const BouncingScrollPhysics(),
                      children: [
                        Column(
                          children: [
                            Card1(),
                            Card2(),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 14.10 * height,
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Center(
                        child: Container(
                          width: 89.7 * width,
                          height: 10.25 * height,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: InkWell(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      TextWidget(
                                          "Chat with Dispatcher",
                                          false,
                                          FontWeight.w800,
                                          1.4,
                                          Constants.cobalt_two,
                                          TextAlign.center,
                                          'pnregular'),
                                      TextWidget(
                                          "Still there an issue?",
                                          false,
                                          FontWeight.w500,
                                          1.1,
                                          Constants.pinegreen,
                                          TextAlign.start,
                                          'pnregular'),
                                    ],
                                  ),
                                  Spacer(),
                                  Container(
                                    margin: EdgeInsets.only(top: 15),
                                    height: 6.4 * height,
                                    width: 12.82 * width,
                                    child: Stack(
                                      children: [
                                        Align(
                                          child: Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                'assets/chat.png',
                                                height: 30,
                                                width: 30,
                                              )
                                            ],
                                          ),
                                          alignment: Alignment.center,
                                        ),
                                        Align(
                                          alignment: Alignment.topRight,
                                          child: CircleAvatar(
                                            radius: 8,
                                            child: TextWidget(
                                                "1",
                                                false,
                                                FontWeight.w100,
                                                0.5,
                                                Colors.white,
                                                TextAlign.center,
                                                'AvenirBold'),
                                          ),
                                        )
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(200),
                                        color: Constants.faddedred),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
