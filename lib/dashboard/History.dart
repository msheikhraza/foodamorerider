import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/Wallet.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:intl/intl.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  List<ModelListRiderRecord> modelOfRecord = [];
  bool loader = false;

  DateTime  dateget =  DateTime.now();
  var date1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getRecordFunction();
    date1 = new DateTime(dateget.year, dateget.month, dateget.day);
  }

  void getRecordFunction() async {

    FirebaseFirestore.instance
        .collection("RiderRidestotal")
        .get()
        .then((value) {

      modelOfRecord = [];
      value.docs.forEach((element) {
        if (FirebaseAuth.instance.currentUser!.uid ==
            element.data()['Riderid'].toString()) {
          setState(() {
            modelOfRecord.add(ModelListRiderRecord(
              customerId: element.data()['Customerid'],
              riderId: element.data()['Riderid'],
              orderId: element.data()['orderid'],
              dateFormat: DateTime.parse(
                element.data()['dateoforder'],
              ),
              priceOrder: element.data()['priceoforder'],
              vendorName: element.data()['vendorname'],
            ));
          });
          modelOfRecord.sort((a,b)=> b.dateFormat.compareTo(a.dateFormat));

        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldState,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 80,
                      width: 500,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: () =>
                                    _scaffoldState.currentState!.openDrawer(),
                                child: Image.asset(
                                  'assets/drawer.png',
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                              Spacer(),

                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: InkWell(
                                  child: InkWell(
                                    child: TextWidget(
                                        "History",
                                        false,
                                        FontWeight.w500,
                                        1.5,
                                        Colors.white,
                                        TextAlign.center,
                                        'pnregular'),
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                Wallet()));
                                  },
                                ),
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                height: 30,
                                color: Constants.faddedred,
                                width: 30,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 1.4,
                  // color: Colors.black,

                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SingleChildScrollView(

                      child: Container(
                        height: MediaQuery.of(context).size.height / 1.4,
                        child:  Container(
                          height: 200,
                          margin: EdgeInsets.only(top: 10),
                          child: ListView(

                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: TextWidget(
                                   "Today",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.black,
                                    TextAlign.start,
                                    'pnregular'),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              Container(

                                child: ListView.builder(
                                  shrinkWrap: true,
                                    itemCount: modelOfRecord.length,
                                    itemBuilder: (BuildContext context, int index){
                                  return  modelOfRecord[
                                  index]
                                      .dateFormat
                                      .isAfter(DateTime
                                      .now()
                                      .subtract(Duration(
                                      days:
                                      1)))
                                      ?

                                    Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 18.0),
                                    child: Column(
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        modelOfRecord[
                                        index]
                                            .dateFormat
                                            .isAfter(DateTime
                                            .now()
                                            .subtract(Duration(
                                            days:
                                            1)))
                                            ?    Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(20),
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color:
                                                Colors.grey.withOpacity(0.5),
                                                spreadRadius: 2,
                                                blurRadius: 2,
                                                offset: Offset(0,
                                                    1), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          child: Column(
                                            children: [

                                              Padding(
                                                padding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 10),
                                                child: Container(
                                                  margin:
                                                  EdgeInsets.only(top: 20),
                                                  width: double.infinity,
                                                  height: 70,
                                                  child: Row(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        height: 30,
                                                        width: 30,
                                                        child: Column(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                          children: [
                                                            Image.asset(
                                                              'assets/shape.png',
                                                              height: 20,
                                                              width: 20,
                                                            )
                                                          ],
                                                        ),
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                            color: Constants
                                                                .faddedred),
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.only(
                                                            left: 10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                          children: [
                                                            TextWidget(
                                                                modelOfRecord[
                                                                index]
                                                                    .dateFormat
                                                                    .isAfter(DateTime
                                                                    .now()
                                                                    .subtract(Duration(
                                                                    days:
                                                                    1)))
                                                                     ?modelOfRecord[
                                                                index]
                                                                    .vendorName: "",

                                                                false,
                                                                FontWeight.w500,
                                                                1.2,
                                                                Constants
                                                                    .pinegreen,
                                                                TextAlign.start,
                                                                'AvenirDemi'),
                                                            TextWidget(
                                                                modelOfRecord[
                                                                index]
                                                                    .dateFormat
                                                                    .isAfter(DateTime
                                                                    .now()
                                                                    .subtract(Duration(
                                                                    days:
                                                                    2)))
                                                                    ?
                                                                "Order ID# " +
                                                                    modelOfRecord[
                                                                    index]
                                                                        .orderId: "",
                                                                false,
                                                                FontWeight.w500,
                                                                0.9,
                                                                Colors.black,
                                                                TextAlign.start,
                                                                'pnregular'),
                                                            TextWidget(
                                                                modelOfRecord[
                                                                index]
                                                                    .dateFormat
                                                                    .isAfter(DateTime
                                                                    .now()
                                                                    .subtract(Duration(
                                                                    days:
                                                                    1)))
                                                                    ?




                                                                "Date:" +
                                                                    DateFormat('d MMM, yyyy')
                                                                        .format( modelOfRecord[
                                                                    index]
                                                                        .dateFormat): "",
                                                                false,
                                                                FontWeight.w500,
                                                                0.9,
                                                                Colors.black,
                                                                TextAlign.start,
                                                                'pnregular'),
                                                          ],
                                                        ),
                                                      ),
                                                      Spacer(),
                                                      TextWidget(
                                                          modelOfRecord[
                                                          index]
                                                              .dateFormat
                                                              .isAfter(DateTime
                                                              .now()
                                                              .subtract(Duration(
                                                              days:
                                                              1)))
                                                              ?

                                                          "\$" +
                                                              modelOfRecord[
                                                              index]
                                                                  .priceOrder.toString(): "",
                                                          false,
                                                          FontWeight.w500,
                                                          1.4,
                                                          Constants.pinegreen,
                                                          TextAlign.start,
                                                          'AvenirDemi'),
                                                    ],
                                                  ),
                                                ),
                                              ),

                                            ],
                                          ),
                                          ): Container()
                                      ],
                                    ),
                                  ): Container(

                                    child: Center(child: null),);
                                })
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: TextWidget(
                                    "Yesterday",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.black,
                                    TextAlign.start,
                                    'pnregular'),
                              ),

                              Container(
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: modelOfRecord.length,
                                      itemBuilder: (BuildContext context, int index){
                                        return Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 18.0),
                                          child: Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              modelOfRecord[
                                              index]
                                                  .dateFormat
                                                  .isAfter(DateTime
                                                  .now()
                                                  .subtract(Duration(
                                                  days:
                                                  2)))
                                                  ?    Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(20),
                                                  color: Colors.white,
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color:
                                                      Colors.grey.withOpacity(0.5),
                                                      spreadRadius: 2,
                                                      blurRadius: 2,
                                                      offset: Offset(0,
                                                          1), // changes position of shadow
                                                    ),
                                                  ],
                                                ),
                                                child: Column(
                                                  children: [

                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 10),
                                                      child: Container(
                                                        margin:
                                                        EdgeInsets.only(top: 20),
                                                        width: double.infinity,
                                                        height: 70,
                                                        child: Row(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment.start,
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.start,
                                                          children: [
                                                            Container(
                                                              height: 30,
                                                              width: 30,
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                                crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                                children: [
                                                                  Image.asset(
                                                                    'assets/shape.png',
                                                                    height: 20,
                                                                    width: 20,
                                                                  )
                                                                ],
                                                              ),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(10),
                                                                  color: Constants
                                                                      .faddedred),
                                                            ),
                                                            Padding(
                                                              padding:
                                                              const EdgeInsets.only(
                                                                  left: 10),
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                                crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                                children: [
                                                                  TextWidget(
                                                                      modelOfRecord[
                                                                      index]
                                                                          .dateFormat
                                                                          .isAfter(DateTime
                                                                          .now()
                                                                          .subtract(Duration(
                                                                          days:
                                                                          2)))
                                                                          ?modelOfRecord[
                                                                      index]
                                                                          .vendorName: "",

                                                                      false,
                                                                      FontWeight.w500,
                                                                      1.2,
                                                                      Constants
                                                                          .pinegreen,
                                                                      TextAlign.start,
                                                                      'AvenirDemi'),
                                                                  TextWidget(
                                                                      modelOfRecord[
                                                                      index]
                                                                          .dateFormat
                                                                          .isAfter(DateTime
                                                                          .now()
                                                                          .subtract(Duration(
                                                                          days:
                                                                          2)))
                                                                          ?
                                                                      "Order ID# " +
                                                                          modelOfRecord[
                                                                          index]
                                                                              .orderId: "",
                                                                      false,
                                                                      FontWeight.w500,
                                                                      0.7,
                                                                      Colors.black,
                                                                      TextAlign.start,
                                                                      'pnregular'),
                                                                  TextWidget(
                                                                      modelOfRecord[
                                                                      index]
                                                                          .dateFormat
                                                                          .isAfter(DateTime
                                                                          .now()
                                                                          .subtract(Duration(
                                                                          days:
                                                                          3)))
                                                                          ?




                                                                      "Date:" +
                                                                          DateFormat('d MMM, yyyy')
                                                                              .format( modelOfRecord[
                                                                          index]
                                                                              .dateFormat).toString(): "",
                                                                      false,
                                                                      FontWeight.w500,
                                                                      0.7,
                                                                      Colors.black,
                                                                      TextAlign.start,
                                                                      'pnregular'),
                                                                ],
                                                              ),
                                                            ),
                                                            Spacer(),
                                                            TextWidget(
                                                                modelOfRecord[
                                                                index]
                                                                    .dateFormat
                                                                    .isAfter(DateTime
                                                                    .now()
                                                                    .subtract(Duration(
                                                                    days:
                                                                    2)))
                                                                    ?

                                                                "\$" +
                                                                    modelOfRecord[
                                                                    index]
                                                                        .priceOrder.toString(): "",
                                                                false,
                                                                FontWeight.w500,
                                                                1.4,
                                                                Constants.pinegreen,
                                                                TextAlign.start,
                                                                'AvenirDemi'),
                                                          ],
                                                        ),
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              ): Container()
                                            ],
                                          ),
                                        );
                                      })
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: TextWidget(
                                    "Past Orders",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.black,
                                    TextAlign.start,
                                    'pnregular'),

                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom:18.0),
                                child: Container(
                                  height: MediaQuery.of(context).size.height /2,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        physics: AlwaysScrollableScrollPhysics(),
                                        itemCount: modelOfRecord.length,
                                        itemBuilder: (BuildContext context, int index){
                                          return Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 18.0),
                                            child: Column(
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                modelOfRecord[
                                                index]
                                                    .dateFormat
                                                .isBefore(DateTime
                                                    .now()
                                                    .subtract(Duration(
                                                    days:
                                                    3)))
                                                    ?    Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                    BorderRadius.circular(20),
                                                    color: Colors.white,
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                        Colors.grey.withOpacity(0.5),
                                                        spreadRadius: 2,
                                                        blurRadius: 2,
                                                        offset: Offset(0,
                                                            1), // changes position of shadow
                                                      ),
                                                    ],
                                                  ),
                                                  child: Column(
                                                    children: [

                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.symmetric(
                                                            horizontal: 10,
                                                            vertical: 10),
                                                        child: Container(
                                                          margin:
                                                          EdgeInsets.only(top: 20),
                                                          width: double.infinity,
                                                          height: 70,
                                                          child: Row(
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment.start,
                                                            mainAxisAlignment:
                                                            MainAxisAlignment.start,
                                                            children: [
                                                              Container(
                                                                height: 30,
                                                                width: 30,
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                                  crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                                  children: [
                                                                    Image.asset(
                                                                      'assets/shape.png',
                                                                      height: 20,
                                                                      width: 20,
                                                                    )
                                                                  ],
                                                                ),
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                    BorderRadius
                                                                        .circular(10),
                                                                    color: Constants
                                                                        .faddedred),
                                                              ),
                                                              Padding(
                                                                padding:
                                                                const EdgeInsets.only(
                                                                    left: 10),
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                                  crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                                  children: [
                                                                    TextWidget(
                                                                        modelOfRecord[
                                                                        index]
                                                                            .dateFormat
                                                                            .isBefore(DateTime
                                                                            .now()
                                                                            .subtract(Duration(
                                                                            days:
                                                                            3)))
                                                                            ?modelOfRecord[
                                                                        index]
                                                                            .vendorName: "",

                                                                        false,
                                                                        FontWeight.w500,
                                                                        1.2,
                                                                        Constants
                                                                            .pinegreen,
                                                                        TextAlign.start,
                                                                        'AvenirDemi'),
                                                                    TextWidget(
                                                                        modelOfRecord[
                                                                        index]
                                                                            .dateFormat
                                                                            .isBefore(DateTime
                                                                            .now()
                                                                            .subtract(Duration(
                                                                            days:
                                                                            3)))
                                                                            ?
                                                                        "Order ID# " +
                                                                            modelOfRecord[
                                                                            index]
                                                                                .orderId: "",
                                                                        false,
                                                                        FontWeight.w500,
                                                                        0.7,
                                                                        Colors.black,
                                                                        TextAlign.start,
                                                                        'pnregular'),
                                                                    TextWidget(
                                                                        modelOfRecord[
                                                                        index]
                                                                            .dateFormat
                                                                            .isBefore(DateTime
                                                                            .now()
                                                                            .subtract(Duration(
                                                                            days:
                                                                            3)))
                                                                            ?




                                                                        "Date:" +
                                                                            DateFormat('d MMM, yyyy')
                                                                                .format( modelOfRecord[
                                                                            index]
                                                                                .dateFormat).toString(): "",
                                                                        false,
                                                                        FontWeight.w500,
                                                                        0.7,
                                                                        Colors.black,
                                                                        TextAlign.start,
                                                                        'pnregular'),
                                                                  ],
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              TextWidget(
                                                                  modelOfRecord[
                                                                  index]
                                                                      .dateFormat
                                                                      .isBefore(DateTime
                                                                      .now()
                                                                      .subtract(Duration(
                                                                      days:
                                                                      3)))
                                                                      ?

                                                                  "\$" +
                                                                      modelOfRecord[
                                                                      index]
                                                                          .priceOrder.toString(): "",
                                                                  false,
                                                                  FontWeight.w500,
                                                                  1.4,
                                                                  Constants.pinegreen,
                                                                  TextAlign.start,
                                                                  'AvenirDemi'),
                                                            ],
                                                          ),
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                ): Container()
                                              ],
                                            ),
                                          );
                                        })
                                ),
                              ),
                            ],
                          ),
                        ),



                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ModelListRiderRecord {
  var orderId;
  var riderId;
  var customerId;
  var vendorName;
  var priceOrder;
  var dateFormat;

  ModelListRiderRecord(
      {this.orderId,
      this.riderId,
      this.customerId,
      this.vendorName,
      this.priceOrder,
      this.dateFormat});
}
