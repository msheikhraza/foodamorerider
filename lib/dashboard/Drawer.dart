import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Statuss.dart';
import 'package:foodamorerider/dashboard/NoPendingDeliveries.dart';
import 'package:foodamorerider/dashboard/Wallet.dart';
import 'package:foodamorerider/dashboard/History.dart';
import 'package:foodamorerider/dashboard/Maps.dart';
import 'package:foodamorerider/provider/userprovider.dart';
import 'package:foodamorerider/splash.dart';
import 'package:provider/provider.dart';

import 'bottom_nav.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {

  void logout() async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => SplashScreen()));
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // final userinfo = Provider.of<UserProvider>(context, listen: false);
    // userinfo.getRiderInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.faddedred,
      width: MediaQuery.of(context).size.width / 1.6,
      child: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 150,
              color: Constants.faddedred,
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10,left: 10),
                    child: Image.asset(
                      'assets/profiledp.png',
                      height: 60,
                      width: 60,
                    ),
                  ),


                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: TextWidget(
                        "Hi",
                        false,
                        FontWeight.w200,
                        1.9,
                        Colors.black,
                        TextAlign.center,
                        'AvenirNextBold'),
                  ),
                  SizedBox(width: 10,),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: TextWidget(
                        "John,",
                        false,
                        FontWeight.w200,
                        1.9,
                        Colors.black,
                        TextAlign.center,
                        'AvenirNextBold'),
                  ),




                ],

              ),
              //
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   children: [
              //     Padding(
              //       padding: const EdgeInsets.only(top: 10, left: 10),
              //       child:

                    //
                    // Consumer<UserProvider>(
                    //   builder: (context, getpic, _) {
                    //     return CachedNetworkImage(
                    //       height: 60,
                    //       width: 60,
                    //       imageUrl: getpic.imageGet!,
                    //       placeholder: (context, url) =>
                    //           CircularProgressIndicator(),
                    //       errorWidget: (context, url, error) =>
                    //           Icon(Icons.error),
                    //     );
                    //
                    //     //   Image.network(
                    //     //   getpic.imageGet!,
                    //     //   height: 60,
                    //     //   width: 60,
                    //     // );
                    //   },
                    // ),
                //  ),
                  // Consumer<UserProvider>(builder: (context, getName, _) {
                  //   return Container(
                  //     margin: EdgeInsets.only(top: 20),
                  //     child: getName.name == null || getName.name == ""
                  //         ? TextWidget("Hi", false, FontWeight.w200, 1.9,
                  //             Colors.black, TextAlign.center, 'AvenirNextBold')
                  //         : TextWidget(
                  //             "Hi ${getName.name}",
                  //             false,
                  //             FontWeight.w200,
                  //             1.9,
                  //             Colors.black,
                  //             TextAlign.center,
                  //             'AvenirNextBold'),
                  //   );
                  // })
              //   ],
              // ),
            ),
            Container(
              height: 600,
              color: Colors.white,

//parent
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                bottomnav(index: 0)));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/chargingCircle.png',
                            color: Constants.coolBlue,
                            height: 25,
                            width: 25,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          TextWidget("Status", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi')
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                deliveriesScreen()));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/deliveryMotorbike.png',
                            height: 25,
                            width: 35,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          TextWidget("Deliveries", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi')
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => Maps()));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/nounMapp.png',
                            height: 25,
                            width: 35,
                            color: Constants.faddedred,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          TextWidget("Maps", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi')
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => History()));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/Historylarge.png',
                            height: 25,
                            width: 35,
                            color: Constants.faddedred,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          TextWidget("History", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi')
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => Wallet()));
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/nounDollarWalletlarge.png',
                            height: 25,
                            width: 35,
                            color: Constants.faddedred,
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          TextWidget("Wallet", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi')
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(
                          'assets/nounDollarWalletlarge.png',
                          height: 25,
                          width: 35,
                          color: Constants.faddedred,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        InkWell(
                          onTap: (){
                            logout();
                          },
                          child: TextWidget("Logout", false, FontWeight.w300, 1,
                              Colors.black, TextAlign.center, 'AvenirDemi'),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
