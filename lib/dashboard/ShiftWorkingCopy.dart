import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';

import 'HelpLine.dart';

class shiftWorking extends StatefulWidget {
  const shiftWorking({Key? key}) : super(key: key);

  @override
  _shiftWorkingState createState() => _shiftWorkingState();
}

class _shiftWorkingState extends State<shiftWorking> {
  bool isSwitched = false;
  var textValue = 'Switch is off';

  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    } else {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 320,
                        width: 500,
                        child: Image.asset('assets/designClipart.png'),
                        decoration: BoxDecoration(
                            color: Constants.faddedred,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(60),
                              bottomRight: Radius.circular(60),
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 50, left: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: ()=> _scaffoldState.currentState!.openDrawer(),

                                  child: Image.asset(
                                    'assets/drawer.png',
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                                Spacer(),
                                TextWidget(
                                    "Status",
                                    false,
                                    FontWeight.w500,
                                    1.8,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                                Spacer(),
                                Image.asset(
                                  'assets/nounSupport.png',
                                  height: 30,
                                  width: 30,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 20, right: 15),
                              child: Container(
                                height: 60,
                                width: 400,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        width: 130,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: Constants.faddedred.withOpacity(0.2),
                                            borderRadius:
                                            BorderRadius.circular(10)),
                                        child: Center(
                                          child: TextWidget(
                                              "Working",
                                              false,
                                              FontWeight.w400,
                                              1.3,
                                              Colors.black,
                                              TextAlign.center,
                                              'pnregular'),
                                        ),
                                      ),
                                    ),
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.all(18.0),
                                      child: TextWidget(
                                          "Request-Break",
                                          false,
                                          FontWeight.w500,
                                          1.3,
                                          Constants.faddedred,
                                          TextAlign.center,
                                          'AvenirBold'),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 40),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: InkWell(
                                  child: TextWidget(
                                      "Current Shifts",
                                      false,
                                      FontWeight.w500,
                                      1.5,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                HelpLine()));
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Divider(),
                            InkWell(
                              child: Container(
                                margin: EdgeInsets.only(right: 16),
                                height: 266,
                                width: 335,
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Container(
                                                height: 100,
                                                width: 70,
                                                decoration: BoxDecoration(
                                                    color: Constants.faddedred.withOpacity(0.2),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 10.0),
                                                      child: TextWidget(
                                                          "Sep",
                                                          false,
                                                          FontWeight.w500,
                                                          1.0,
                                                          Constants.faddedred.withOpacity(0.7),
                                                          TextAlign.center,
                                                          'pnregular'),
                                                    ),
                                                    TextWidget(
                                                        "07",
                                                        false,
                                                        FontWeight.w500,
                                                        2.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                    TextWidget(
                                                        "Today",
                                                        false,
                                                        FontWeight.w500,
                                                        1.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
//height
                                          height: 100,
                                          width: 200,
                                          //color: Colors.black,

                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 8.0),
                                                    child: TextWidget(
                                                        "24:49  -  23:45",
                                                        false,
                                                        FontWeight.w500,
                                                        1.2,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 8),
                                                    child: TextWidget(
                                                        "(56mins)",
                                                        false,
                                                        FontWeight.w500,
                                                        1.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                  ),
                                                ],
                                              ),

                                              Container(
                                                height: 30,
                                                width: 200,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 8.0),
                                                      child: TextWidget(
                                                          "Vancouver",
                                                          false,
                                                          FontWeight.w300,
                                                          1.2,
                                                          Colors.black,
                                                          TextAlign.center,
                                                          'pnregular'),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  height: 30,
                                                  width: 150,
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        Icons
                                                            .check_circle_outline,
                                                        size: 20,
                                                        color: Constants
                                                            .cobalt_two,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 8.0),
                                                        child: TextWidget(
                                                            "Ending Soon",
                                                            false,
                                                            FontWeight.w500,
                                                            1.3,
                                                            Constants
                                                                .cobalt_two,
                                                            TextAlign.center,
                                                            'pnregular'),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 20,),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      child: Divider(
                                        height: 20,
                                        thickness: 2,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: TextWidget(
                                                "Available for Shift Extension",
                                                false,
                                                FontWeight.w500,
                                                1.3,
                                                Colors.black,
                                                TextAlign.center,
                                                'pnregular'),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Switch(
                                          onChanged: toggleSwitch,
                                          value: isSwitched,
                                          activeColor: Colors.green,
                                          activeTrackColor:
                                              Constants.whiteFifteen,
                                          inactiveThumbColor:
                                              Constants.whiteFifteen,
                                          inactiveTrackColor:
                                              Constants.rosyPink,
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {},
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
