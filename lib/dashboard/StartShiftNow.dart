import 'dart:ui';
import 'package:custom_check_box/custom_check_box.dart';
import 'package:dropdown_below/dropdown_below.dart';
import 'package:flutter/cupertino.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/ShiftStatusWorking.dart';
import 'package:foodamorerider/dashboard/ShiftBooked.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

import 'CustomDialog_2.dart';

class statusbooked extends StatefulWidget {
  const statusbooked({Key? key}) : super(key: key);

  @override
  _statusbookedState createState() => _statusbookedState();
}

var checkBoxValue = false;

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}

class _statusbookedState extends State<statusbooked>
    with SingleTickerProviderStateMixin {
  List _testList = [
    {'no': 1, 'keyword': 'blue'},
    {'no': 2, 'keyword': 'black'},
    {'no': 3, 'keyword': 'red'}
  ];
  List<DropdownMenuItem<Object?>> _dropdownTestItems = [];
  var _selectedTest;

  @override
  void initState() {
    _dropdownTestItems = buildDropdownTestItems(_testList);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<DropdownMenuItem<Object?>> buildDropdownTestItems(List _testList) {
    List<DropdownMenuItem<Object?>> items = [];
    for (var i in _testList) {
      items.add(
        DropdownMenuItem(
          value: i,
          child: Text(i['keyword']),
        ),
      );
    }
    return items;
  }

  bool _switchValue = true;

  onChangeDropdownTests(selectedTest) {
    print(selectedTest);
    setState(() {
      _selectedTest = selectedTest;
    });
  }

  TabController? _tabController;
  String? _chosenValue;
  @override
  String? _selected;
  List<Map> _myJson = [
    {'id': '1', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '2', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '3', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '4', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'}
  ];
  List<String> temperature = ['Bag Type', "Big", 'Large', 'D']; // Option 2
  String? selectedLocation;
  bool isSwitched = false;
  var textValue = 'Switch is OFF';

  void toggleSwitch(bool value) {

    if(isSwitched == false)
    {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    }
    else
    {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    print(height.toString()+   width.toString());
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer:    DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 37.1*height,
                        width: 125*width,
                        child: Image.asset('assets/designClipart.png'),
                        decoration: BoxDecoration(
                            color: Constants.faddedred,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(60),
                              bottomRight: Radius.circular(60),
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30, left: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                  child: Image.asset(
                                    'assets/drawer.png',
                                    height: 5.4*height,
                                    width: 5.4*width,
                                  ),
                                ),
                                Spacer(),
                                TextWidget(
                                    "Status",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                                Spacer(),
                                Image.asset(
                                  'assets/nounSupport.png',
                                  height: 5.4*height,
                                  width: 5.4*width,
                                ),
                                SizedBox(
                                  width: 2.5*width,
                                ),
                                SizedBox(
                                  width: 3.75*width,
                                ),
                              ],
                            ),
                            SizedBox(height: 2.5*height,),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 20, right: 22,left: 10),
                              child: Container(
                                height: 7.6*height,
                                width: 100*width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 10,top:8),
                                      width: 35*width,
                                      height: 5.7*height,
                                      decoration: BoxDecoration(
                                          color: Constants.faddedred.withOpacity(0.2),
                                          borderRadius:
                                          BorderRadius.circular(10)
                                      ),
                                      child: Center(
                                        child: TextWidget(
                                            "Not Working",
                                            false,
                                            FontWeight.w700,
                                            1.1,
                                            Colors.black,
                                            TextAlign.center,
                                            'AvenirNextRegular'),
                                      ),
                                    ),
                                    Spacer(),

                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20,left: 20),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: TextWidget(
                                    "Upcoming Shift",
                                    false,
                                    FontWeight.w500,
                                    1.3,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Divider(),
                            InkWell(
                              child: Container(
                                margin: EdgeInsets.only(right: 16),
                                height: 34.1*height,
                                width: 83.7*width,
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(12.0),
                                              child: Container(
                                                height: 12.8*height,
                                                width: 17.5*width,
                                                decoration: BoxDecoration(
                                                    color: Constants.faddedred.withOpacity(0.2),
                                                    borderRadius:
                                                    BorderRadius.circular(20)
                                                ),
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.only(
                                                          top: 10.0),
                                                      child: TextWidget(
                                                          "Sep",
                                                          false,
                                                          FontWeight.w800,
                                                          1.0,
                                                          Constants.faddedred.withOpacity(0.7),
                                                          TextAlign.center,
                                                          'AvenirReg'),
                                                    ),
                                                    TextWidget(
                                                        "07",
                                                        false,
                                                        FontWeight.w500,
                                                        2.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                    TextWidget(
                                                        "Today",
                                                        false,
                                                        FontWeight.w400,
                                                        1.0,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'pnregular'),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
//height
                                          height: 12.8*height,
                                          width: 50*width,
                                          //color: Colors.black,

                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        top: 2),
                                                    child: TextWidget(
                                                        "24:49  -  23:45",
                                                        false,
                                                        FontWeight.w700,
                                                        1.2,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'AvenirDemi'),
                                                  ),
                                                  TextWidget(
                                                      "(56mins)",
                                                      false,
                                                      FontWeight.w500,
                                                      1.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'pnregular'),
                                                ],
                                              ),
                                              Container(
                                                height: 3.8*height,
                                                width: 50*width,
                                                child: Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.only(
                                                          top: 2.0),
                                                      child: TextWidget(
                                                          "Vancouver",
                                                          false,
                                                          FontWeight.w100,
                                                          1.0,
                                                          Colors.black,
                                                          TextAlign.center,
                                                          'AvenirNextRegular'),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  height: 3.8*height,
                                                  width: 37.5*width,
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        Icons.calendar_today,
                                                        size: 20,
                                                        color:
                                                        Constants.faddedred,
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets
                                                            .only(
                                                            left: 2.0),
                                                        child: TextWidget(
                                                            "Booked",
                                                            false,
                                                            FontWeight.w500,
                                                            1.1,
                                                            Constants.faddedred,
                                                            TextAlign.center,
                                                            'AvenirNextRegular'),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 18),
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: TextWidget(
                                            "Are you Ready?",
                                            false,
                                            FontWeight.w700,
                                            1.0,
                                            Colors.black,
                                            TextAlign.center,
                                            'AvenirDemi'),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 3.92*height,
                                    ),
                                    InkWell(
                                      child: Container(
                                        width:
                                        MediaQuery.of(context).size.width / 1.4,
                                        height: 6.6*height,
                                        child: Center(
                                          child: TextWidget(
                                              "Start Shift Now",
                                              false,
                                              FontWeight.w500,
                                              1,
                                              Colors.white,
                                              TextAlign.center,
                                              'AvenirDemi'),
                                        ),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(11),
                                            color: Constants.faddedred.withOpacity(0.9)),
                                      ),
                                    )
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset:
                                      Offset(
                                          0, 3), // changes position of shadow

                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return CustomDialog_2();
                                    });
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}