import 'dart:async';

import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/MessageCenter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';


class Maps extends StatefulWidget {
  const Maps({Key? key}) : super(key: key);

  @override
  _MapsState createState() => _MapsState();
}

class _MapsState extends State<Maps> {

  Set<Marker> _markers={};
  double CAMERA_ZOOM = 8;
  double CAMERA_TILT = 0;
  double CAMERA_BEARING = 30;
  LocationData? currentLocation;
  Position? _currentPosition;
  String? _currentAddress;
   LatLng sourcelocation  = LatLng( 24.795671733957743, 67.05058978961291);






  void initState() {
    // TODO: implement initState

    //
    // _getCurrentLocation();
    //
    super.initState();
  }

  Completer<GoogleMapController> _controlle = Completer();
  late GoogleMapController _controller;

  void _onMapCreated(GoogleMapController _cntlr)
  {
    _controlle.complete(_cntlr);
    _controller = _cntlr;

  }
  _getCurrentLocation() {
    Geolocator
        .getCurrentPosition()
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        print(_currentPosition.toString() + "Address");
      });

      // _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    CameraPosition initialLocation = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: CAMERA_BEARING,
        tilt: CAMERA_TILT,
        target: sourcelocation
    );
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: ListView(
              children: [
                Stack(
             

                  children: [
                    Container(
                      height: 10.25*height,
                      width: 125*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                child: Image.asset(
                                  'assets/drawer.png',
                                    height: 5.4*height,
                                    width: 5.4*width
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: InkWell(
                                  child: TextWidget(
                                      "Maps",
                                      false,
                                      FontWeight.w500,
                                      1.6,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),

                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (BuildContext context) => MessageCenter()));
                                  },
                                ),

                                // onTap: (){
                                //   Navigator.of(context).push(MaterialPageRoute(
                                //       builder: (BuildContext context) => newOrder()));
                                // },
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                  height: 5.4*height,
                                  width: 5.4*width
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),



                  ],
                ),



                Container(

                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/1.31,

                 child: GoogleMap(
                   padding: EdgeInsets.only(top: 200),
                   myLocationButtonEnabled: true,
                   initialCameraPosition: initialLocation,
                   mapType: MapType.normal,
                   onMapCreated: _onMapCreated,
                   myLocationEnabled: true,
                   markers: _markers,zoomControlsEnabled: true,

                 ),
                  ),


              ],
            ),
          ),
        ),
      ),
    );
  }
}
