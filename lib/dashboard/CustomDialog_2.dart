import 'dart:ui';

import 'package:custom_check_box/custom_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

import 'ShiftStatusWorking.dart';

class CustomDialog_2 extends StatefulWidget {

  const CustomDialog_2({Key? key}) : super(key: key);

  @override
  _CustomDialog_2State createState() => _CustomDialog_2State();



}
var checkBoxValue = false;

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}


class _CustomDialog_2State extends State<CustomDialog_2> with SingleTickerProviderStateMixin {


  List _testList = [
    {'no': 1, 'keyword': 'blue'},
    {'no': 2, 'keyword': 'black'},
    {'no': 3, 'keyword': 'red'}
  ];
  List<DropdownMenuItem<Object?>> _dropdownTestItems = [];
  var _selectedTest;

  @override
  void initState() {
    _dropdownTestItems = buildDropdownTestItems(_testList);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<DropdownMenuItem<Object?>> buildDropdownTestItems(List _testList) {
    List<DropdownMenuItem<Object?>> items = [];
    for (var i in _testList) {
      items.add(
        DropdownMenuItem(
          value: i,
          child: Text(i['keyword']),
        ),
      );
    }
    return items;
  }

  bool _switchValue = true;

  onChangeDropdownTests(selectedTest) {
    print(selectedTest);
    setState(() {
      _selectedTest = selectedTest;
    });
  }

  TabController? _tabController;
  String? _chosenValue;
  @override
  String? _selected;
  List<Map> _myJson = [
    {'id': '1', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '2', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '3', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'},
    {'id': '4', 'image': 'assets/deliverybike.png', 'name': 'deliverybike'}
  ];
  List<String> temperature = ['Bag Type', "Big", 'Large', 'D']; // Option 2
  String? selectedLocation;
  bool isSwitched = false;
  var textValue = 'Switch is OFF';

  void toggleSwitch(bool value) {

    if(isSwitched == false)
    {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    }
    else
    {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }

  Widget build(BuildContext context) {

    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: contentBox(context),
      ),
    );
  }
  contentBox(context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return SingleChildScrollView(



      child: Container(
        height: 64.10*height,

        width: 100*width,

        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(
                  0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                color: Colors.white,
                height: 5.1*height,
                width: 75*width,
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.center,
                  mainAxisAlignment:
                  MainAxisAlignment.center,
                  children: [
                    TextWidget(
                        "Choose Equipment",
                        false,
                        FontWeight.w500,
                        1.6,
                        Colors.black,
                        TextAlign.center,
                        'AvenirDemi'),
                  ],
                ),
              ),
              SizedBox(
                height: 2.5*height,
              ),
              Container(
                height: 6.4*height,

                padding: EdgeInsets.symmetric(
                    horizontal: 5),
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(10),
                    border: Border.all(
                        color: Colors.grey)),
                child: Row(
                  children: [
                    Container(
                      width: 7.5*width,
                      child: Image.asset(
                        'assets/deliverybike.png',
                        height: 3.8*height,
                        width: 5*width,
                      ),
                    ),
                    SizedBox(
                      width: 2.5*width,
                    ),
                    Container(
                      child:
                      DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Container(
                            width: 170,
                            child: Text(
                                'Vehicle Type'),
                          ),
                          // Not necessary for Option 1
                          value: selectedLocation,
                          onChanged: (newValue) {
                            setState(() {
                              selectedLocation =
                                  newValue.toString();
                            });
                          },
                          items: temperature
                              .map((location) {
                            return DropdownMenuItem(
                              child:
                              new Text(location),
                              value: location,
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.5*height,
              ),
              Container(
                height: 6.4*height,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(10),
                    border: Border.all(
                        color: Colors.grey)),
                child: Row(
                  children: [
                    Container(
                      width: 30,
                      child: Image.asset(
                        'assets/Shopping.png',
                        height: 2.5*height,
                        width: 2.5*width,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child:
                      DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Container(
                            width: 170,
                            child: Text(
                                'Bag Type'),
                          ),
                          // Not necessary for Option 1
                          value: selectedLocation,
                          onChanged: (newValue) {
                            setState(() {
                              selectedLocation =
                                  newValue.toString();
                            });
                          },
                          items: temperature
                              .map((location) {
                            return DropdownMenuItem(
                              child:
                              new Text(location),
                              value: location,
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.5*height,
              ),
              Container(
                height: 6.4*height,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey)),
                child: Row(
                  children: [
                    Container(
                        width: 30,
                        child: Image.asset(
                          'assets/Temperature.png',
                          height: 30,
                          width: 30,
                        )),

                    Container(
                      child:  Text(
                          'Body Temperature C  '),

                    ),
                  ],
                ),
              ),
              Row(
                crossAxisAlignment:
                CrossAxisAlignment.start,
                mainAxisAlignment:
                MainAxisAlignment.start,
                children: [
                  Transform.scale(
                    scale: 1.0,
                    child: Container(
                      margin: EdgeInsets.only(right: 2),
                      child: CustomCheckBox(
                        value: checkBoxValue,
                        splashColor:
                        Colors.red.withOpacity(0.4),
                        borderColor:
                        Constants.faddedred,
                        tooltip: 'Custom Check Box',
                        splashRadius: 40,
                        onChanged: (val) {
                          //do your stuff here
                          setState(() {
                            checkBoxValue = val;
                          });
                        },
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        right: 5, top: 15),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: TextWidget(
                          "I agree to the",
                          false,
                          FontWeight.w400,
                          0.9,
                          Colors.black,
                          TextAlign.start,
                          'pnboldd'),
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 15),
                    child: TextWidget(
                        "Privacy Policy",
                        false,
                        FontWeight.w900,
                        1,
                        Constants.faddedred,
                        TextAlign.start,
                        'pnboldd'),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 1),
                child: Divider(
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10 , bottom: 10),
                child: Row(
                  children: [
                    Container(

                      child: TextWidget(
                          "I am avaliable to extend my shift",
                          false,
                          FontWeight.w600,
                          0.9,
                          Constants.faddedred,
                          TextAlign.start,
                          'pnboldd'),
                    ),
                    Spacer(),
                    Container(
                      height: 30,
                      width: 50,
                      child: FlutterSwitch(
                        value: isSwitched,
                        activeColor: Constants.faddedred,
                        inactiveColor: Constants.pigpink,
                        onToggle: (val) {
                          setState(() {
                            isSwitched = val;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),

              Divider(
                thickness: 2,
              ),
              SizedBox( height: 1.3*height,),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 0),
                child: Row(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  mainAxisAlignment:
                  MainAxisAlignment.start,
                  children: [
                    Transform.scale(
                      scale: 1.0,
                      child: Container(
                        width: 32.5*width,
                        height: 6.4*height,
                        child: Center(
                          child: TextWidget(
                              "Cancel",
                              false,
                              FontWeight.w500,
                              1,
                              Colors.white,
                              TextAlign.center,
                              'AvenirDemi'),
                        ),
                        decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(
                                11),
                            color:
                            Constants.cobalt_two),
                      ),
                    ),
                    Spacer(),
                    InkWell(
                        child: Container(
                          width: 32.5*width,
                          height: 6.4*height,
                          child: Center(
                            child: TextWidget(
                                "Start",
                                false,
                                FontWeight.w500,
                                1,
                                Colors.white,
                                TextAlign.center,
                                'AvenirDemi'),
                          ),
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.circular(11),
                              color: Constants.faddedred),
                        ),


                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => OnGoingScreen()));
                        }




                    ),
                    SizedBox(height: 30,),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}