import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:geocoding/geocoding.dart' as geo;
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:foodamorerider/dashboard/GoToVendor.dart';
import 'package:location/location.dart' as locationpackage;
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator_platform_interface/src/enums/location_accuracy.dart' as mark;
class newOrder extends StatefulWidget {
  const newOrder({Key? key}) : super(key: key);

  @override
  _newOrderState createState() => _newOrderState();
}

class _newOrderState extends State<newOrder> {
  int _currentStep = 2;

  var getvendorlat = RideDatalist[0].vendorLatitude;
  var getvendorlong = RideDatalist[0].vendorLongitude;
  var getridelat = RideDatalist[0].customerLatitude;
  var getriderlong = RideDatalist[0].customerLongitude;
List getorder=[];
   LatLng sourcelocation =LatLng(double.parse(RideDatalist[0].vendorLatitude),
       double.parse(RideDatalist[0].vendorLongitude));
  LatLng DEST_LOCATION  =LatLng(double.parse(RideDatalist[0].customerLatitude),
      double.parse(RideDatalist[0].customerLongitude));
  Set<Polyline> _polylines = {};

  List<LatLng> polylineCoordinates = [];
  Completer<GoogleMapController> _controlle = Completer();
  PolylinePoints polylinePoints = PolylinePoints();
   double CAMERA_ZOOM = 13;
   double CAMERA_TILT = 0;
   double CAMERA_BEARING = 30;
  String? VendorAddress, CustomerAddress;
  locationpackage.Location _location = locationpackage.Location();
  Set<Marker> _markers={};
  var status;
  FirebaseDatabase db = FirebaseDatabase.instance;
  void _onMapCreated(GoogleMapController _cntlr)
  {
    _controlle.complete(_cntlr);
    _controller = _cntlr;
    setMapPins();
    setPolylines();
  }

  void setMapPins() {
    setState(() {// source pin
      _markers.add(Marker(
          markerId: MarkerId("sourcePin"),
          position: sourcelocation,
      ));
      // destination pin
      _markers.add(Marker(
          markerId: MarkerId("destPin"),
          position: DEST_LOCATION,

      ));
    });
  }
  locationpackage.Location _locationTracker = locationpackage.Location();

  StreamSubscription? _locationSubscription;

  void getCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      setState(() {
        long = position.longitude;
        lat = position.latitude;

      });


    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }
  void updatePinOnMap() async {

    // create a new CameraPosition instance
    // every time the location changes, so the camera
    // follows the pin as it moves with an animation
    CameraPosition cPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,
      target: LatLng(double.parse(currentLocation!.latitude.toString()),
          double.parse(currentLocation!.longitude.toString())),
    );
    //final GoogleMapController controller = await _controller.getZoomLevel();
    // controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
   // do this inside the setState() so Flutter gets notified
    // that a widget update is due
    setState(() {
      // updated position
      var pinPosition = LatLng(double.parse(currentLocation!.latitude.toString()),

          double.parse(currentLocation!.longitude.toString()));

      // the trick is to remove the marker (by id)
      // and add it again at the updated location

    });
  }


  setPolylines() async {
    PolylineResult result= await
    polylinePoints.getRouteBetweenCoordinates(
        googleAPIKey,
        PointLatLng(  sourcelocation.latitude ,
          sourcelocation.longitude,),
        PointLatLng(    DEST_LOCATION.latitude,
            DEST_LOCATION.longitude ),
      );
    if(result.points.isNotEmpty){
      // loop through all PointLatLng points and convert them
      // to a list of LatLng, required by the Polyline
      result.points.forEach((PointLatLng point){
        polylineCoordinates.add(
            LatLng(point.latitude, point.longitude));
      });
    }
    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      Polyline polyline = Polyline(
          polylineId: PolylineId("poly"),
          color: Colors.red,
          points: polylineCoordinates,
        width: 2

      );

      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
      _polylines.add(polyline);
    });
  }


  static final kInitialPosition = LatLng(double.parse(RideDatalist[0].vendorLatitude),
      double.parse(RideDatalist[0].vendorLongitude));

  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  late GoogleMapController _controller;


  Future<void> GetVendorLatLong(lattitude, longitude) async {
    print(lattitude.toString() + " " + " " + longitude.toString());

    List<geo.Placemark> placemarks =
        await geo.placemarkFromCoordinates(lattitude, 74.0073255);
    print(placemarks);
    geo.Placemark place = placemarks[0];
    String address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {
      VendorAddress = address;
    });
  }

  Future<void> GetCustomerLatLong(lattitude, longitude) async {
    List<geo.Placemark> placemarks =
        await geo.placemarkFromCoordinates(lattitude, longitude);

    geo.Placemark place = placemarks[0];
    String address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {
      CustomerAddress = address;
    });
  }
  locationpackage.LocationData? currentLocation;
  locationpackage.LocationData? destinationLocation;

  locationpackage.Location? location = new locationpackage.Location();
  void setInitialLocation() async {
    // set the initial location by pulling the user's
    // current location from the location's getLocation()
    currentLocation = await location!.getLocation();

    // hard-coded destination for this example
    destinationLocation = locationpackage.LocationData.fromMap({
      "latitude": DEST_LOCATION.latitude,
      "longitude": DEST_LOCATION.longitude
    });
  }
  @override
  void initState() {
    // TODO: implement initState

    GetVendorLatLong(double.parse(RideDatalist[0].vendorLatitude),
        double.parse(RideDatalist[0].vendorLongitude));
    getCurrentLocation();
    GetCustomerLatLong(double.parse(RideDatalist[0].customerLatitude),
        double.parse(RideDatalist[0].customerLongitude));

    location!.onLocationChanged.listen((locationpackage.LocationData cLoc) {
      // cLoc contains the lat and long of the
      // current user's position in real time,
      // so we're holding on to it
      currentLocation = cLoc;
      updatePinOnMap();
    });

    setInitialLocation();
    getorder.add( RideDatalist[0].orderDetails);

    productIdList =[];
    priceList =[];
    quantityList =[];
    nameList =[];

    getorder.forEach((element) {

      productIdList = element['productId'];
      priceList = element['price'];
      quantityList = element['Quantity'];
      nameList = element['name'];



    });

    print(productIdList.toString()+"------------product");
    print(priceList.toString()+"------------price");
    print(quantityList.toString()+"------------quantity");
    print(nameList.toString()+"------------name");





    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    CameraPosition initialLocation = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: CAMERA_BEARING,
        tilt: CAMERA_TILT,
        target: sourcelocation
    );
    if (currentLocation != null) {
      initialLocation = CameraPosition(
          target: LatLng(double.parse(currentLocation!.latitude.toString()),

              double.parse(currentLocation!.longitude.toString())),
          zoom: CAMERA_ZOOM,
          tilt: CAMERA_TILT,
          bearing: CAMERA_BEARING
      );
    }

    var height = MediaQuery.of(context).size.height / 100;
    var width = MediaQuery.of(context).size.width / 100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldState,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 80,
                      width: 500,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: () =>
                                    _scaffoldState.currentState!.openDrawer(),
                                child: Image.asset(
                                  'assets/drawer.png',
                                  height: 5.4 * height,
                                  width: 5.4 * width,
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: InkWell(
                                  child: TextWidget(
                                      "Deliveries",
                                      false,
                                      FontWeight.w500,
                                      1.5,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),
                                  // onTap: () {
                                  //   Navigator.of(context).push(
                                  //       MaterialPageRoute(
                                  //           builder: (BuildContext context) =>
                                  //               VendorsScreen()));
                                  // },
                                ),
                                // onTap: () {
                                //   Navigator.of(context).push(MaterialPageRoute(
                                //       builder: (BuildContext context) =>
                                //           newOrder()));
                                // },
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                height: 5.4 * height,
                                width: 5.4 * width,
                              ),
                              SizedBox(
                                width: 2.5 * width,
                              ),
                              SizedBox(
                                width: 3.75 * width,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.92 * height,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 3.84 * height,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextWidget("New Order", false, FontWeight.w500, 1.4,
                            Colors.black, TextAlign.center, 'AvenirNextBold'),
                        Spacer(),
                        TextWidget("Decline", false, FontWeight.w300, 1.2,
                            Colors.black, TextAlign.center, 'AvenirNextBold'),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 2.56 * height,
                ),
                Container(
                  width: 80 * width,
                  height: 25.64 * height,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: GoogleMap(
                    padding: EdgeInsets.only(top: 200),
                     myLocationButtonEnabled: true,
                    initialCameraPosition: initialLocation,
                    mapType: MapType.normal,
                    polylines: _polylines,
                    onMapCreated: _onMapCreated,
                    myLocationEnabled: true,
                    markers: _markers,zoomControlsEnabled: true,

                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 28.2 * height,
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 12),
                                  height: 3.84 * height,
                                  width: 7.5 * width,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/shape.png',
                                        height: 1.92 * height,
                                        width: 3.75 * width,
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Constants.faddedred),
                                ),
                                SizedBox(
                                  width: 5 * width,
                                ),
                                Container(
                                  width: 62.5 * width,
                                  height: 12.82 * height,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 22),
                                        child: TextWidget(
                                            "Pickup",
                                            false,
                                            FontWeight.w500,
                                            1.1,
                                            Colors.black,
                                            TextAlign.center,
                                            'AvenirNextBold'),
                                      ),
                                      TextWidget(
                                          VendorAddress.toString(),
                                          false,
                                          FontWeight.w500,
                                          0.8,
                                          Colors.black,
                                          TextAlign.center,
                                          'AvenirNextBold'),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 80),
                                        child: TextWidget(
                                            "568 Beatty St, Vancouver, BC V6B 2L3, Canada",
                                            false,
                                            FontWeight.w500,
                                            0.8,
                                            Colors.black,
                                            TextAlign.center,
                                            'pnregular'),
                                      ),
                                    ],
                                  ),
                                ),
                                TextWidget(
                                    "2 min",
                                    false,
                                    FontWeight.w500,
                                    1.0,
                                    Colors.black,
                                    TextAlign.center,
                                    'pnregular'),
                              ],
                            ),
                            Positioned(
                              top: 8 * height,
                              child: Container(
                                margin:
                                    EdgeInsets.only(left: 6 * width, top: 0),
                                height: 12.82 * height,
                                width: 1 * width,
                                color: Colors.red,
                              ),
                            ),
                            Positioned(
                              top: 15 * height,
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 10),
                                    height: 3.84 * height,
                                    width: 7.5 * width,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          'assets/path.png',
                                          height: 1.92 * height,
                                          width: 3.75 * width,
                                        )
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Constants.faddedred),
                                  ),
                                  SizedBox(width: 5 * width),
                                  Container(
                                    width: 62.5 * width,
                                    height: 12 * height,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15),
                                          child: TextWidget(
                                              "Drop-off",
                                              false,
                                              FontWeight.w500,
                                              1.3,
                                              Colors.black,
                                              TextAlign.center,
                                              'AvenirNextBold'),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 80),
                                          child: TextWidget(
                                              CustomerAddress.toString(),
                                              false,
                                              FontWeight.w500,
                                              0.8,
                                              Colors.black,
                                              TextAlign.center,
                                              'pnregular'),
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextWidget(
                                      "6 min",
                                      false,
                                      FontWeight.w500,
                                      1.0,
                                      Colors.black,
                                      TextAlign.center,
                                      'pnregular'),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: StepProgressIndicator(
                    totalSteps: 100,
                    currentStep: 70,
                    size: 3,
                    padding: 0,
                    unselectedColor: Constants.whiteFifteen,
                    selectedColor: Constants.faddedred,
                    roundedEdges: Radius.circular(10),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Container(
                    margin: EdgeInsets.only(top: 2.56 * height),
                    width: MediaQuery.of(context).size.width,
                    height: 8.97 * height,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            child: Column(
                          children: [
                            TextWidget("You earn", false, FontWeight.w400, 1.3,
                                Colors.black, TextAlign.center, 'pnregular'),
                            TextWidget(
                                "\$" + RideDatalist[0].totalPrice.toString(),
                                false,
                                FontWeight.w200,
                                2.0,
                                Constants.faddedred,
                                TextAlign.center,
                                'AvenirNextBold'),
                          ],
                        )),
                        Spacer(),
                        InkWell(
                          child: Container(
                            width: 35 * width,
                            height: 7.05 * height,
                            child: Center(
                              child: TextWidget(
                                  "Accept",
                                  false,
                                  FontWeight.w500,
                                  1.3,
                                  Colors.white,
                                  TextAlign.center,
                                  'AvenirDemi'),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(11),
                                color: Constants.faddedred),
                          ),
                          onTap: () {
String orderid;
    db.ref().child("riderrequests").child(RideDatalist[0].orderId.toString()).once().then((snapshot) {
      print('Data : ${snapshot.snapshot.value}');
    Map<dynamic, dynamic> values = snapshot.snapshot.value as Map<dynamic, dynamic>;
      print(values["status"]);
      status = values["status"];
      print(status + "Status   ");
    if(status == "Pending"){

      VendorAdd = values['vendorName'];
      orderid = values['orderId'];
       db.ref().child("riderrequests").child(RideDatalist[0].orderId.toString()).update(
     {    'status' : 'Accept'}



       ).then((value) {

 db.ref().child("ridertovendor").child(FirebaseAuth.instance.currentUser!.uid).set({
  "riderlat":  lat.toString(),
  "riderlong": long.toString(),
   "vendorlat":RideDatalist[0].vendorLatitude,
   "vendorlong":RideDatalist[0].vendorLongitude,
  "orderid":orderid,
  "arrivedstatustovendor": "false"
});
         Navigator.of(context).push(MaterialPageRoute(
             builder: (BuildContext context) =>
                 VendorsScreen()));
       });
      // db.ref().child("riderrequests").update({
      //
      // })
    }
    // else{
    //
    // }

    });
  }
                        //  },
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
