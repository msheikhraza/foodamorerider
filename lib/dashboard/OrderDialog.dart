import 'dart:ui';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/StartShiftNow.dart';

import 'NewOrder.dart';

class OrderDialogBox extends StatefulWidget {
  @override
  _OrderDialogBoxState createState() => _OrderDialogBoxState();
}

class _OrderDialogBoxState extends State<OrderDialogBox> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: contentBox(context),
      ),
    );
  }

  contentBox(context) {
    return SingleChildScrollView(
        child: Container(
            width: MediaQuery.of(context).size.width / 1.0,
            height: 200,
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Image.asset(
                  'assets/Notification.png',
                  height: 50,
                  width: 50,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextWidget("New Ride", false, FontWeight.w700, 1.5, Colors.black,
                  TextAlign.center, 'AvenirDemi'),
              // Align(
              //   alignment: Alignment.topLeft,
              //   child: Padding(
              //     padding: const EdgeInsets.only(top: 15.0, left: 12),
              //     child: TextWidget("REMINDER:", false, FontWeight.w500, 1.2,
              //         Colors.black, TextAlign.center, 'AvenirDemi'),
              //   ),
              // ),
              // Padding(
              //   padding: const EdgeInsets.only(top: 10, left: 12),
              //   child: Align(
              //     alignment: Alignment.topLeft,
              //     child: TextWidget(
              //         "Your shift is starting in 30 minutes.",
              //         false,
              //         FontWeight.w200,
              //         1,
              //         Constants.pinegreen.withOpacity(0.7),
              //         TextAlign.center,
              //         'AvenirDemi'),
              //   ),
              // ),
              SizedBox(
                height: 30,
              ),
              InkWell(
                onTap: () {

                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => newOrder()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.4,
                  height: 50,
                  child: Center(
                    child: TextWidget("View", false, FontWeight.w500, 1,
                        Colors.white, TextAlign.center, 'AvenirDemi'),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      color: Constants.faddedred),
                ),
              )
            ]),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            )));
  }
}
