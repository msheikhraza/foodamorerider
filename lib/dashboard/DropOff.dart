import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/Maps.dart';
import 'package:foodamorerider/widgets/appnavigaton.dart';
import 'package:intl/intl.dart';

import 'bottom_nav.dart';

class drop_off extends StatefulWidget {
  const drop_off({Key? key}) : super(key: key);

  @override
  _drop_offState createState() => _drop_offState();
}

class _drop_offState extends State<drop_off> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  FirebaseDatabase db = FirebaseDatabase.instance;
var date;
var formatter;
  String formattedDate ="";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    date= DateTime.now();
     formatter= DateFormat('yyyy-MM-dd');
    formattedDate = formatter.format(date);

  }
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: 10.25*height,
                    width: 125*width,
                    decoration: BoxDecoration(
                        color: Constants.faddedred,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        )),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 15),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            InkWell(
                              onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                              child: Image.asset(
                                'assets/drawer.png',
                                  height: 5.4*height,
                                  width: 5.4*width
                              ),
                            ),
                            Spacer(),
                            InkWell(
                              child: InkWell(
                                child: TextWidget(
                                    "Drop-off",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                              ),

                              // onTap: (){
                              //   Navigator.of(context).push(MaterialPageRoute(
                              //       builder: (BuildContext context) => newOrder()));
                              // },
                            ),
                            Spacer(),
                            Image.asset(
                              'assets/nounSupport.png',
                                height: 5.4*height,
                                width: 5.4*width
                            ),
                            SizedBox(
                              width: 2.5 * width,
                            ),
                            SizedBox(
                              width: 2.5 * width,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 3.0 * height,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height:  3.0 * height,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      TextWidget("Drop off at 23:13", false, FontWeight.w100,
                          1.2, Colors.black, TextAlign.center, 'AvenirDemi'),

                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 8.9*height,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: TextWidget(
                            RideDatalist[0].customerName,//customer name
                            false,
                            FontWeight.w100,
                            1.4,
                            Colors.black,
                            TextAlign.center,
                            'AvenirBold'),
                      ),
                      Spacer(),
                      Container(
                        height: 4.8  * height,
                        width: 10.2 * width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/locate.png',
                              height: 2.4 * height,
                              width: 5.1 * width,
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(200),
                            color: Constants.faddedred),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        height: 4.8  * height,
                        width: 10.2 * width,
                        child: Stack(
                          children: [
                            Align(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/chat.png',
                                    height: 2.4 * height,
                                    width: 5.1 * width,
                                  )
                                ],
                              ),
                              alignment: Alignment.center,
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: CircleAvatar(
                                backgroundColor: Colors.black,
                                radius: 8,
                                child: TextWidget(
                                    "1",
                                    false,
                                    FontWeight.w100,
                                    0.5,
                                    Colors.white,
                                    TextAlign.center,
                                    'AvenirBold'),
                              ),
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(200),
                            color: Constants.faddedred),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        height: 4.8  * height,
                        width: 10.2 * width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/call.png',
                              height: 2.4 * height,
                              width: 5.1 * width,
                            )
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(200),
                            color: Constants.faddedred),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 52.15*height,
                width: 87.5*width,
                child: Padding(
                  padding: const EdgeInsets.only(top: 15, left: 10, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWidget("Order Details", false, FontWeight.w400, 1.1,
                          Colors.black, TextAlign.center, 'pnregular'),
                      SizedBox(height: 8,),
                      TextWidget(productIdList[0], false, FontWeight.w700,
                          1.5, Colors.black, TextAlign.center, 'AvenirDemi'),
                      SizedBox(height: 8,),
                      TextWidget(
                          RideDatalist[0].vendorName,
                          false,
                          FontWeight.w800,
                          0.9,
                          Colors.black,
                          TextAlign.center,
                          'AvenirNextRegular'),
                      SizedBox(height: 5,),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: TextWidget(nameList.length.toString()+" Items", false, FontWeight.w400,
                            1.1, Colors.black, TextAlign.center, 'pnregular'),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Divider(
                          height: 12,
                          thickness: 1,
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [

                          Container(
                            width: 70*width,
                            height: 23*height,
                            child: ListView.builder(
                                itemCount: nameList.length,
                                itemBuilder: (BuildContext context, int i) {
                                  return Container(

                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [

                                        Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              width: 25,
                                              height: 25,

                                              child: Center(
                                                child: TextWidget(
                                                    quantityList[i].toString(),
                                                    false,
                                                    FontWeight.w700,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'venirDemi'),
                                              ),

                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: Constants.faddedred),

                                            ),
                                            Container(
                                              width: 50.5*width,
                                              height: 6.41*height,
                                              child: TextWidget(

                                                  "   "+nameList[i].toString(),
                                                  false,
                                                  FontWeight.w700,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.start,
                                                  'AvenirDemi'),
                                            ),

                                            Spacer(),
                                            TextWidget(
                                                "\$"+priceList[i].toString(),
                                                false,
                                                FontWeight.w700,
                                                1.0,
                                                Colors.black,
                                                TextAlign.center,
                                                'venirDemi'),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Divider(thickness: 2,),

                                      ],
                                    ),
                                  );
                                }
                            ),
                          ),                        ],
                      ),
                      Divider(
                        height: 2,
                        thickness: 2,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        height: 7.3 * height,
                        //color: Colors.yellow,

                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 0),
                              width: 110,
                              height: double.infinity,
                              //color: Colors.yellow,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TextWidget(
                                      "Payment",
                                      false,
                                      FontWeight.w200,
                                      1.1,
                                      Constants.pinegreen.withOpacity(0.7),
                                      TextAlign.start,
                                      'AvenirDemi'),
                                  // SizedBox(
                                  //   height: 10,
                                  // ),
                                  SizedBox(height: 10,),
                                  TextWidget(
                                      "Paid Online",
                                      false,
                                      FontWeight.w500,
                                      1.3,
                                      Colors.black,
                                      TextAlign.start,
                                      'AvenirDemi')
                                ],
                              ),
                            ),
                            Spacer(),
                            Container(
                              margin: EdgeInsets.only(right: 20,top: 10),
                              child: Column(
                                children: [
                                  TextWidget(
                                      "\$"+RideDatalist[0].totalPrice.toString(),
                                      false,
                                      FontWeight.w500,
                                      2.1,
                                      Constants.faddedred,
                                      TextAlign.start,
                                      'AvenirNextBold'),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 8.97*height,
              ),
              InkWell(
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.1,
                  height: 58,
                  child: Center(
                    child: TextWidget("Drop-off", false, FontWeight.w500, 1.2,
                        Colors.white, TextAlign.center, 'AvenirDemi'),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      color: Constants.faddedred),
                ),
                onTap: () {

                  FirebaseFirestore.instance.collection('Orders').doc(RideDatalist[0].orderId.toString()).update({
                    "Status":"Delivered"
                  }).then((result){
                    print("new USer true");
                  }).catchError((onError){
                    print("onError");
                  });
                  // db.ref().child("riderrequests").child(RideDatalist[0].orderId.toString()).update(
                  //     {    'status' : 'Delivered'}
                  //
                  //
                  //
                  // );




                  FirebaseFirestore.instance.collection("RiderRidestotal").doc().set({
                    "Riderid": FirebaseAuth.instance.currentUser!.uid,
                    "Customerid": RideDatalist[0].customerId,
                    "priceoforder":  RideDatalist[0].totalPrice,
                    "dateoforder":formattedDate,
                    "orderid": RideDatalist[0].orderId,
                    "vendorname": RideDatalist[0].vendorName


                  }).then((value) {
                    AppNavigation.navigateTo(context, bottomnav(index: 0));
                  });

                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
