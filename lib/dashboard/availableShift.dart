import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/ShiftBooked.dart';
import 'package:foodamorerider/dashboard/Statuss.dart';
import 'package:foodamorerider/dashboard/StartShiftNow.dart';
import 'package:foodamorerider/dashboard/Statuss.dart';

import 'bottom_nav.dart';

class avShifts extends StatefulWidget {
  const avShifts({Key? key}) : super(key: key);

  @override
  _avShiftsState createState() => _avShiftsState();
}

class _avShiftsState extends State<avShifts> {
  DateTime selectedDate = DateTime.now(); // TO tracking date

  int currentDateSelectedIndex = 0; //For Horizontal Date

  ScrollController scrollController =
  ScrollController(); //To Track Scroll of ListView

  List<String> listOfMonths = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  List<String> listOfDays = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    print(height.toString()+ width.toString());
    return Container(
      height:MediaQuery.of(context).size.height,
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.whitetwo,
          body: Container(
            height: MediaQuery.of(context).size.height,

            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 32.0*height,
                      width: 128.2*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                  child: Image.asset(
                                    'assets/iconArrowBack.png',
                                    height: 6.9*height,
                                    width: 6.9*width,
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (BuildContext context) => HomeScreen()));
                                  }

                              ),
                              Spacer(),
                              InkWell(
                                child: TextWidget(
                                    "Available Shifts",
                                    false,
                                    FontWeight.w500,
                                    1.6,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                                onTap: () {
                                  setState(() {
                                    if (currentDateSelectedIndex == 0) {
                                      currentDateSelectedIndex = 1;
                                    } else {
                                      currentDateSelectedIndex = 0;
                                    }
                                  });
                                },
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/iconFilter.png',
                                height: 7.5*height,
                                width: 7.5*width,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              SizedBox(
                                width: 3.8*width,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        //To Show Current Date
                        SizedBox(
                          height: 12.8*height,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: [
                              Image.asset(
                                'assets/invalidName.png',
                                height: 4.2*height,
                                width: 4.2*width,
                              ),
                              Spacer(),
                              Container(
                                  height: 6.41*height,
                                  margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  child: Text(
                                    listOfMonths[selectedDate.month - 1] +
                                        ', ' +
                                        selectedDate.year.toString(),
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  )),
                              Spacer(),
                              Image.asset(
                                'assets/invalidName.png',
                                height: 4.2*height,
                                width: 4.2*width,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 1.28*height),
                        //To show Calendar Widget
                        Container(
                            height: 7.69*height,
                            child: Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 20),
                              child: Container(
                                  child: ListView.separated(
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return SizedBox(width: 10);
                                    },
                                    itemCount: 365,
                                    controller: scrollController,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return InkWell(
                                        onTap: () {
                                          setState(() {
                                            currentDateSelectedIndex = index;
                                            selectedDate = DateTime.now()
                                                .add(Duration(days: index));
                                          });
                                        },
                                        child: Container(
                                          height:6.41*height,
                                          width: 10.25*width,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(8),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Constants.faddedred,
                                                    offset: Offset(1, 1),
                                                    blurRadius: 6)
                                              ],
                                              color: currentDateSelectedIndex ==
                                                  index
                                                  ? Constants.rosepink
                                                  .withOpacity(0.9)
                                                  : Constants.faddedred
                                                  .withOpacity(0.9)),
                                          child: Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                height: 2,
                                              ),
                                              Text(
                                                listOfDays[DateTime.now()
                                                    .add(Duration(
                                                    days: index))
                                                    .weekday -
                                                    1]
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color:
                                                    currentDateSelectedIndex ==
                                                        index
                                                        ? Colors.white
                                                        : Constants.softPink),
                                              ),
                                              SizedBox(
                                                height: 0.6*height,
                                              ),
                                              Text(
                                                DateTime.now()
                                                    .add(Duration(days: index))
                                                    .day
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w700,
                                                    color:
                                                    currentDateSelectedIndex ==
                                                        index
                                                        ? Colors.white
                                                        : Constants.softPink),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  )),
                            )),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.56*height,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: MediaQuery.of(context).size.height/1.8,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        currentDateSelectedIndex == 1
                            ? Container(
                          height: 100,
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                const EdgeInsets.only(right: 190.0),
                                child: TextWidget(
                                    "My Shifts (01)",
                                    false,
                                    FontWeight.w200,
                                    1.4,
                                    Colors.black,
                                    TextAlign.center,
                                    'AvenirDemi'),
                              ),
                              Container(
                                height: 70,
                                width: MediaQuery.of(context).size.width / 1.1,
                                //color: Colors.grey,

                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.1),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Row(
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: TextWidget(
                                                "Vancouver",
                                                false,
                                                FontWeight.w200,
                                                1.2,
                                                Colors.black,
                                                TextAlign.center,
                                                'AvenirDemi'),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10, left: 10),
                                            child: TextWidget(
                                                "17:45 - 23:45 / 6h",
                                                false,
                                                FontWeight.w100,
                                                1.0,
                                                Colors.black,
                                                TextAlign.center,
                                                'AvenirDemi'),
                                          ),
                                        ],
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          height: 7.69*height,
                                          width: 30.7*width,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            color: Constants.cobalt_two.withOpacity(0.9),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.withOpacity(0.2),
                                                spreadRadius: 5,
                                                blurRadius: 7,
                                                offset: Offset(0,
                                                    3), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              TextWidget(
                                                  "Not Started",
                                                  false,
                                                  FontWeight.w300,
                                                  1.1,
                                                  Colors.white,
                                                  TextAlign.center,
                                                  'pnreguler'),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        SizedBox(
                          height: 2.56*height,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 130.0),
                          child: TextWidget(
                              "Available Shifts (07)",
                              false,
                              FontWeight.w200,
                              1.4,
                              Colors.black,
                              TextAlign.center,
                              'AvenirDemi'),
                        ),



                        Container(
                          //secondshift
                          height: MediaQuery.of(context).size.height/3.1,
                          child: SingleChildScrollView(
                            child: Column(

                              children: [

                                Container(
                                  height: 70,
                                  width: MediaQuery.of(context).size.width / 1.1,
                                  //color: Colors.grey,

                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(
                                              0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0, top: 10),
                                              child: TextWidget(
                                                  "Vancouver",
                                                  false,
                                                  FontWeight.w200,
                                                  1.2,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, left: 10),
                                              child: TextWidget(
                                                  "17:45 - 23:45 / 6h",
                                                  false,
                                                  FontWeight.w100,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: InkWell(
                                            onTap: () {
                                              Navigator.of(context).push(MaterialPageRoute(
                                                  builder: (BuildContext context) => bottomnav(index: 0,check: 0,)));
                                            },
                                            child: Container(
                                              height: 7.69*height,
                                              width: 30*width,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5),
                                                color: Constants.faddedred.withOpacity(0.9),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey.withOpacity(0.2),
                                                    spreadRadius: 5,
                                                    blurRadius: 7,
                                                    offset: Offset(0,
                                                        3), // changes position of shadow
                                                  ),
                                                ],
                                              ),
                                              child: Center(
                                                child: TextWidget(
                                                    "Take Shift",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'pnreguler'),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 70,
                                  width: MediaQuery.of(context).size.width / 1.1,
                                  //color: Colors.grey,

                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(
                                              0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0, top: 10),
                                              child: TextWidget(
                                                  "Vancouver",
                                                  false,
                                                  FontWeight.w200,
                                                  1.2,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, left: 10),
                                              child: TextWidget(
                                                  "17:45 - 23:45 / 6h",
                                                  false,
                                                  FontWeight.w100,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Container(
                                            height: 7.69*height,
                                            width: 30*width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              color: Constants.faddedred.withOpacity(0.9),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.2),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                TextWidget(
                                                    "Take Shift",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'pnreguler'),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 70,
                                  width: MediaQuery.of(context).size.width / 1.1,
                                  //color: Colors.grey,

                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(
                                              0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0, top: 10),
                                              child: TextWidget(
                                                  "Vancouver",
                                                  false,
                                                  FontWeight.w200,
                                                  1.2,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, left: 10),
                                              child: TextWidget(
                                                  "17:45 - 23:45 / 6h",
                                                  false,
                                                  FontWeight.w100,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Container(
                                            height: 7.69*height,
                                            width: 30*width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              color: Constants.faddedred.withOpacity(0.9),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.2),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                TextWidget(
                                                    "Take Shift",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'pnreguler'),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 1.2*height,
                                ),
                                Container(
                                  height: 8.97*height,
                                  width: MediaQuery.of(context).size.width / 1.1,
                                  //color: Colors.grey,

                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(
                                              0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0, top: 10),
                                              child: TextWidget(
                                                  "Vancouver",
                                                  false,
                                                  FontWeight.w200,
                                                  1.2,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, left: 10),
                                              child: TextWidget(
                                                  "17:45 - 23:45 / 6h",
                                                  false,
                                                  FontWeight.w100,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Container(
                                            height: 7.69*height,
                                            width: 30*width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              color: Constants.faddedred.withOpacity(0.9),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.2),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                TextWidget(
                                                    "Take Shift",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'pnreguler'),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 1.2*height,
                                ),
                                Container(
                                  height: 8.97*height,
                                  width: MediaQuery.of(context).size.width / 1.1,
                                  //color: Colors.grey,

                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(
                                              0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: [
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0, top: 10),
                                              child: TextWidget(
                                                  "Vancouver",
                                                  false,
                                                  FontWeight.w200,
                                                  1.2,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, left: 10),
                                              child: TextWidget(
                                                  "17:45 - 23:45 / 6h",
                                                  false,
                                                  FontWeight.w100,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'AvenirDemi'),
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Container(
                                            height: 7.69*height,
                                            width: 30*width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              color: Constants.faddedred.withOpacity(0.9),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.2),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: [
                                                TextWidget(
                                                    "Take Shift",
                                                    false,
                                                    FontWeight.w300,
                                                    1.1,
                                                    Colors.white,
                                                    TextAlign.center,
                                                    'pnreguler'),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}