import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/CustomDialog.dart';
import 'package:foodamorerider/dashboard/StartShiftNow.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';

class StartShift extends StatefulWidget {
  const StartShift({Key? key}) : super(key: key);

  @override
  _StartShiftState createState() => _StartShiftState();
}

class _StartShiftState extends State<StartShift> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    print(height.toString()+   width.toString());
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldState,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 35.8*height,
                        width: 125*width,
                        child: Image.asset('assets/designClipart.png'),
                        decoration: BoxDecoration(
                            color: Constants.faddedred,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(60),
                              bottomRight: Radius.circular(60),
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30, left: 15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: () =>
                                      _scaffoldState.currentState!.openDrawer(),
                                  child: Image.asset(
                                    'assets/drawer.png',
                                    height: 5.4*height,
                                    width: 5.4*width,
                                  ),
                                ),
                                Spacer(),
                                Padding(
                                  padding: const EdgeInsets.only(right: 120),
                                  child: TextWidget(
                                      "Status",
                                      false,
                                      FontWeight.w500,
                                      1.6,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),
                                ),
                                SizedBox(
                                  width: 2.5*width,
                                ),
                                SizedBox(
                                  width: 3.75*width,
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 20, right: 22,left: 10),
                              child: Container(
                                height: 7.69*height,
                                width: 100*width,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 10,top:8),
                                      width: 35*width,
                                      height: 5.76*height,
                                      decoration: BoxDecoration(
                                          color: Constants.faddedred.withOpacity(0.2),
                                          borderRadius:
                                          BorderRadius.circular(10)
                                      ),
                                      child: Center(
                                        child: TextWidget(
                                            "Not Working",
                                            false,
                                            FontWeight.w700,
                                            1.1,
                                            Colors.black,
                                            TextAlign.center,
                                            'AvenirNextRegular'),
                                      ),
                                    ),
                                    Spacer(),

                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 25,left: 25),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: TextWidget(
                                    "Next Shifts",
                                    false,
                                    FontWeight.w500,
                                    1.4,
                                    Colors.white,
                                    TextAlign.center,
                                    'pnregular'),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Divider(),
                            InkWell(
                                child: Container(
                                  margin: EdgeInsets.only(right: 12),
                                  height: 18.58*height,
                                  width: 83.75*width,
                                  child: Row(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(12.0),
                                            child: Container(
                                              height: 12.82*height,
                                              width: 17.5*width,
                                              decoration: BoxDecoration(
                                                  color: Constants.faddedred.withOpacity(0.2),
                                                  borderRadius:
                                                  BorderRadius.circular(20)),
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        top: 10.0),
                                                    child: TextWidget(
                                                        "Sep",
                                                        false,
                                                        FontWeight.w800,
                                                        1.0,
                                                        Constants.faddedred.withOpacity(0.7),
                                                        TextAlign.center,
                                                        'AvenirReg'),
                                                  ),
                                                  TextWidget(
                                                      "07",
                                                      false,
                                                      FontWeight.w500,
                                                      2.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'pnregular'),
                                                  TextWidget(
                                                      "Today",
                                                      false,
                                                      FontWeight.w400,
                                                      1.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'pnregular'),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
//height
                                        height: 12.8*height,
                                        width: 50*width,
                                        //color: Colors.black,

                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 1.0),
                                                  child: TextWidget(
                                                      "24:49  -  23:45",
                                                      false,
                                                      FontWeight.w700,
                                                      1.2,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'AvenirDemi'),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 1),
                                                  child: TextWidget(
                                                      "(56mins)",
                                                      false,
                                                      FontWeight.w500,
                                                      1.0,
                                                      Colors.black,
                                                      TextAlign.center,
                                                      'AvenirDemi'),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              height: 3.8*height,
                                              width: 50*width,
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        top: 1.0),
                                                    child: TextWidget(
                                                        "Vancouver",
                                                        false,
                                                        FontWeight.w100,
                                                        1,
                                                        Colors.black,
                                                        TextAlign.center,
                                                        'AvenirNextRegular'),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Container(

                                                height: 3.8*height,
                                                width: 37.5*width,
                                                child: Row(
                                                  children: [
                                                    Icon(
                                                      Icons.calendar_today,
                                                      size: 20,
                                                      color: Constants.faddedred,
                                                    ),
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets.only(
                                                          left: 2.0,top: 2),
                                                      child: TextWidget(
                                                          "Booked",
                                                          false,
                                                          FontWeight.w500,
                                                          1.2,
                                                          Constants.faddedred,
                                                          TextAlign.center,
                                                          'AvenirNextRegular'),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(0, 3), // changes position of shadow
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                ),
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return CustomDialogBox();
                                      });
                                }

                              //AlertDialog


                            ),
                            SizedBox(
                              height: 2.5*height,
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}


//
//
//   showGeneralDialog(
//   barrierDismissible: true,
//   barrierLabel: '',
//   barrierColor: Colors.black38,
//   transitionDuration:
//   Duration(milliseconds: 500),
//   pageBuilder: (ctx, anim1, anim2) =>
//   AlertDialog(
//   elevation: 2,
//   actions: [
//   FlatButton(
//   onPressed: () {
//   Navigator.of(context).push(
//   MaterialPageRoute(
//   builder:
//   (BuildContext context) =>
//   statusbooked()));
//   },
//   child: Container(
//   width: MediaQuery.of(context)
//       .size
//       .width /
//   1.0,
//   height: 290,
//   child: Column(children: [
//   Padding(
//   padding: const EdgeInsets.only(
//   top: 10),
//   child: Image.asset(
//   'assets/Notification.png',
//   height: 50,
//   width: 50,
//   ),
//   ),
//   TextWidget(
//   "RoadRunner",
//   false,
//   FontWeight.w500,
//   1.3,
//   Colors.black,
//   TextAlign.center,
//   'AvenirDemi'),
//   Align(
//   alignment: Alignment.topLeft,
//   child: Padding(
//   padding: const EdgeInsets.only(
//   top: 5.0),
//   child: TextWidget(
//   "REMINDER:",
//   false,
//   FontWeight.w500,
//   1.1,
//   Colors.black,
//   TextAlign.center,
//   'AvenirDemi'),
//   ),
//   ),
//   Padding(
//   padding: const EdgeInsets.only(
//   top: 10),
//   child: Align(
//   alignment: Alignment.topLeft,
//   child: TextWidget(
//   "Your shift is starting in 30 minutes.:",
//   false,
//   FontWeight.w200,
//   1.0,
//   Constants.whitegrey,
//   TextAlign.center,
//   'AvenirDemi'),
//   ),
//   ),
//   SizedBox(
//   height: 30,
//   ),
//   Container(
//   width: MediaQuery.of(context)
//       .size
//       .width /
//   1.2,
//   height: 45,
//   child: Center(
//   child: TextWidget(
//   "OK",
//   false,
//   FontWeight.w500,
//   1.4,
//   Colors.white,
//   TextAlign.center,
//   'AvenirDemi'),
//   ),
//   decoration: BoxDecoration(
//   borderRadius:
//   BorderRadius.circular(11),
//   color: Constants.faddedred),
//   )
//   ]),
//
//   decoration: BoxDecoration(
//   borderRadius: BorderRadius.circular(20),
//   color: Colors.white,
//   boxShadow: [
//   BoxShadow(
//   color: Colors.grey.withOpacity(0.5),
//   spreadRadius: 5,
//   blurRadius: 7,
//   offset: Offset(
//   0, 3), // changes position of shadow
//   ),
//   ],
//   )
//
//   ),
//
//   // onPressed: () {
//   //   Navigator.of(context).pop();
//   // },
//   ),
//   ],
//   ),
//   transitionBuilder:
//   (ctx, anim1, anim2, child) =>
//   BackdropFilter(
//   filter: ImageFilter.blur(
//   sigmaX: 4 * anim1.value,
//   sigmaY: 4 * anim1.value),
//   child: FadeTransition(
//   child: child,
//   opacity: anim1,
//   ),
//   ),
//   context: context,
//   );
// },