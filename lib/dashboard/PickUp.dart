import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/Models/GetRideData.dart';
import 'package:foodamorerider/dashboard/ArrivedatCustomer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';

class pickupScreen extends StatefulWidget {

  const pickupScreen(this.formattedDate, {Key? key}) : super(key: key);
  final String formattedDate ;
  @override
  _pickupScreenState createState() => _pickupScreenState();
}

class BottomDialog {
  void showBottomDialog(BuildContext context) {
    showGeneralDialog(
      barrierLabel: "showGeneralDialog",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.6),
      transitionDuration: const Duration(milliseconds: 400),
      context: context,
      pageBuilder: (context, _, __) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: _buildDialogContent(context),
        );
      },
      transitionBuilder: (_, animation1, __, child) {
        return SlideTransition(
          position: Tween(
            begin: const Offset(0, 1),
            end: const Offset(0, 0),
          ).animate(animation1),
          child: child,
        );
      },
    );
  }

  Widget _buildDialogContent(BuildContext context) {
    return IntrinsicHeight(
      child: Container(
        width: double.maxFinite,
        clipBehavior: Clip.antiAlias,
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        child: Material(
          child: Column(
            children: [
              TextWidget(
                  "Do you confirm order w0qv-vllh (#1801) pickup?",
                  false,
                  FontWeight.w500,
                  1.2,
                  Colors.black,
                  TextAlign.start,
                  'pnregular'),
              SizedBox(
                height: 10,
              ),
              InkWell(
                  child: Container(
                    width: 350,
                    height: 58,
                    child: Center(
                      child: TextWidget(
                          "Confirm Pickup?",
                          false,
                          FontWeight.w500,
                          1.2,
                          Colors.white,
                          TextAlign.center,
                          'AvenirDemi'),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: Constants.faddedred),
                  ),
                  onTap: () {
                    //  bottomDialog.showBottomDialog(context);
                    FirebaseDatabase db = FirebaseDatabase.instance;
                    db.ref().child("ridertocustomer").child(FirebaseAuth.instance.currentUser!.uid).set({
                      "riderlat":  lat.toString(),
                      "riderid": FirebaseAuth.instance.currentUser!.uid,
                      "riderlong": long.toString(),
                      "customerid": RideDatalist[0].customerId,
                      "customerlat":RideDatalist[0].customerLatitude,
                      "customerlong":RideDatalist[0].customerLongitude,
                      "arrivedstatustocustomer": "false"
                    });


                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => GotoCostumer()));
                  }),
              SizedBox(
                height: 20,
              ),
              InkWell(
                  child: Container(
                    width: 350,
                    height: 58,
                    child: Center(
                      child: TextWidget("Cancel", false, FontWeight.w500, 1.2,
                          Colors.white, TextAlign.center, 'AvenirDemi'),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(11),
                        color: Constants.cobalt_two),
                  ),
                  onTap: () {
                    //  bottomDialog.showBottomDialog(context);
                  }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImage() {
    const image =
        'https://user-images.githubusercontent.com/47568606/134579553-da578a80-b842-4ab9-ab0b-41f945fbc2a7.png';
    return SizedBox(
      height: 88,
      child: Image.network(image, fit: BoxFit.cover),
    );
  }

  Widget _buildContinueText() {
    return const Text(
      'Continue with account',
      style: TextStyle(
        fontSize: 22,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget _buildEmapleText() {
    return const Text(
      'example.com',
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget _buildTextField() {
    const iconSize = 40.0;
    return Container(
      height: 60,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 1, color: Colors.grey.withOpacity(0.4)),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Row(
        children: [
          Container(
            width: iconSize,
            height: iconSize,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.grey[200],
            ),
            child: const Center(
              child: Text('Е'),
            ),
          ),
          const SizedBox(height: 16),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'elisa.g.beckett@gmail.com',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text('**********'),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildContinueButton() {
    return Container(
      height: 40,
      width: double.maxFinite,
      decoration: const BoxDecoration(
        color: Color(0xFF3375e0),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: RawMaterialButton(
        onPressed: () {
          // Navigator.of(context, rootNavigator: true).pop();
        },
        child: const Center(
          child: Text(
            'Continue',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}

class _pickupScreenState extends State<pickupScreen> {
  BottomDialog bottomDialog = BottomDialog();
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
@override
  void initState() {
    // TODO: implement initState


    super.initState();


}
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height /100;
    var width = MediaQuery.of(context).size.width /100;
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 10.25*height,
                      width: 125*width,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: ()=> _scaffoldState.currentState!.openDrawer(),
                                child: Image.asset(
                                  'assets/drawer.png',
                                  height: 5.4*height,
                                  width: 5.4*width,
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: InkWell(
                                  child: TextWidget(
                                      "Pickup",
                                      false,
                                      FontWeight.w500,
                                      1.6,
                                      Colors.white,
                                      TextAlign.center,
                                      'pnregular'),

                                  // onTap: (){
                                  //   Navigator.of(context).push(MaterialPageRoute(
                                  //       builder: (BuildContext context) => VendorsScreen()));
                                  // },
                                ),

                                // onTap: (){
                                //   Navigator.of(context).push(MaterialPageRoute(
                                //       builder: (BuildContext context) => newOrder()));
                                // },
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                height: 5.4*height,
                                width: 5.4*width,
                              ),
                              SizedBox(
                                width: 2.5*width,
                              ),
                              SizedBox(
                                width: 3.75,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.92*height,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 3.8*height,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextWidget("Pickup at  " + widget.formattedDate.toString(), false, FontWeight.w500,
                            1.2, Colors.black, TextAlign.center, 'AvenirDemi'),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 8.9*height,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10,left: 5),
                          child: TextWidget(
                              RideDatalist[0].vendorName.toString(),
                              false,
                              FontWeight.w100,
                              1.3,
                              Colors.black,
                              TextAlign.center,
                              'AvenirBold'),
                        ),
                        Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              padding: EdgeInsets.all(7),
                              child: Image.asset(
                                'assets/locate.png',
                                height: 20,
                                width: 20,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(200),
                                  color: Constants.faddedred),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              height: 40,
                              width: 40,
                              child: Stack(
                                children: [
                                  Align(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          'assets/chat.png',
                                          height: 20,
                                          width: 20,
                                        )
                                      ],
                                    ),
                                    alignment: Alignment.center,
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: CircleAvatar(
                                      backgroundColor: Colors.black,
                                      radius: 8,
                                      child: TextWidget(
                                          "1",
                                          false,
                                          FontWeight.w100,
                                          0.5,
                                          Colors.white,
                                          TextAlign.center,
                                          'AvenirBold'),
                                    ),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(200),
                                  color: Constants.faddedred),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              height: 40,
                              width: 40,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/call.png',
                                    height: 20,
                                    width: 20,
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(200),
                                  color: Constants.faddedred),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 46.15*height,
                  width: 87.5*width,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWidget("Order Details", false, FontWeight.w600, 1.0,
                            Colors.black, TextAlign.center, 'AvenirNextRegular'),
                        SizedBox(height: 2,),
                        TextWidget(RideDatalist[0].orderId.toString(), false, FontWeight.w700,
                            1.4, Colors.black, TextAlign.center, 'AvenirDemi'),
                        SizedBox(height: 2,),
                        TextWidget(
                            RideDatalist[0].customerName.toString(),
                            false,
                            FontWeight.w600,
                            1.0,
                            Colors.black,
                            TextAlign.center,
                            'AvenirNextRegular'),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: TextWidget("${nameList.length} Items", false, FontWeight.w400,
                              1.1, Colors.black, TextAlign.center, 'pnregular'),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 0),
                          child: Divider(
                            height: 40,
                            thickness: 2,
                          ),
                        ),
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                width: 30,
                                height: 17.9*height,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [



                                  ],
                                ),
                              ),
                              Container(
                                width: 70*width,
                                height: 23*height,
                                child: ListView.builder(
                                  itemCount: nameList.length,
                                  itemBuilder: (BuildContext context, int i) {
                                    return Container(

                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [

                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 25,
                                                height: 25,

                                                child: Center(
                                                  child: TextWidget(
                                                      quantityList[i].toString(),
                                                      false,
                                                      FontWeight.w700,
                                                      1.1,
                                                      Colors.white,
                                                      TextAlign.center,
                                                      'venirDemi'),
                                                ),

                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(5),
                                                    color: Constants.faddedred),

                                              ),
                                              Container(
                                                width: 50.5*width,
                                                height: 6.41*height,
                                                child: TextWidget(

                                                    "   "+nameList[i].toString(),
                                                    false,
                                                    FontWeight.w700,
                                                    1.0,
                                                    Colors.black,
                                                    TextAlign.start,
                                                    'AvenirDemi'),
                                              ),

                                              Spacer(),
                                              TextWidget(
                                                  "\$"+priceList[i].toString(),
                                                  false,
                                                  FontWeight.w700,
                                                  1.0,
                                                  Colors.black,
                                                  TextAlign.center,
                                                  'venirDemi'),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Divider(thickness: 2,),

                                        ],
                                      ),
                                    );
                                  }
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 75,
                ),
                InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      height: 58,
                      child: Center(
                        child: TextWidget("Pickup", false, FontWeight.w500, 1.2,
                            Colors.white, TextAlign.center, 'AvenirDemi'),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(11),
                          color: Constants.faddedred),
                    ),
                    onTap: () {
                      bottomDialog.showBottomDialog(context);
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
