import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/Drawer.dart';
import 'package:foodamorerider/dashboard/ShiftWorkingCopy.dart';

class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);

  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.faddedred,
      child: SafeArea(
        child: Scaffold(
          key:_scaffoldState ,
          drawer: DrawerScreen(),
          backgroundColor: Constants.whitetwo,
          body: Container(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: 80,
                      width: 500,
                      decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: ()=> _scaffoldState.currentState!.openDrawer(),

                                child: Image.asset(
                                  'assets/drawer.png',
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: InkWell(
                                  child: InkWell(
                                    child: TextWidget(
                                        "Wallet",
                                        false,
                                        FontWeight.w500,
                                        1.8,
                                        Colors.white,
                                        TextAlign.center,
                                        'pnregular'),
                                  ),
                                  onTap: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                shiftWorking()));
                                  },
                                ),
                              ),
                              Spacer(),
                              Image.asset(
                                'assets/nounSupport.png',
                                height: 30,
                                color: Constants.faddedred,
                                width: 30,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 1.4,
                  // color: Colors.black,

                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                                padding: const EdgeInsets.only(left: 20,top: 10),
                                child: Column(
                                  children: [
                                    Center(
                                      child: TextWidget(
                                          "Current Balance",
                                          false,
                                          FontWeight.w500,
                                          1.3,
                                          Colors.black,
                                          TextAlign.start,
                                          'AvenirDemi'),
                                    ),
                                    SizedBox(height: 10,),
                                    TextWidget(
                                        "-\$236.26",
                                        false,
                                        FontWeight.w500,
                                        2.2,
                                        Colors.black,
                                        TextAlign.start,
                                        'AvenirNextBold'),
                                  ],
                                )),

                            SizedBox(height: 20,),
                            Center(
                              child: Container(
                                width: 360,
                                height: 400,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15,top: 20),
                                      child: TextWidget(
                                          "Recent Transactions",
                                          false,
                                          FontWeight.w500,
                                          1.5,
                                          Colors.black,
                                          TextAlign.left,
                                          'AvenirDemi'),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Container(
                                        width: double.infinity,
                                        height: 70,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Image.asset(
                                                    'assets/shape.png',
                                                    height: 20,
                                                    width: 20,
                                                  )
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Constants.faddedred),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  TextWidget(
                                                      "Collection",
                                                      false,
                                                      FontWeight.w500,
                                                      1.2,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'AvenirDemi'),
                                                  TextWidget(
                                                      "Order# wov-villh",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Colors.black,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                  TextWidget(
                                                      "Date 07 Sep",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                ],
                                              ),
                                            ),
                                            Spacer(),
                                            TextWidget(
                                                "\$42.26",
                                                false,
                                                FontWeight.w500,
                                                1.4,
                                                Constants.pinegreen,
                                                TextAlign.start,
                                                'AvenirDemi'),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      child: Divider(
                                        thickness: 1,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Container(
                                        margin: EdgeInsets.only(top: 20),
                                        width: double.infinity,
                                        height: 70,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Image.asset(
                                                    'assets/shape.png',
                                                    height: 20,
                                                    width: 20,
                                                  )
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Constants.faddedred),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  TextWidget(
                                                      "Collection",
                                                      false,
                                                      FontWeight.w500,
                                                      1.2,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'AvenirDemi'),
                                                  TextWidget(
                                                      "Order# wov-villh",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Colors.black,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                  TextWidget(
                                                      "Date 07 Sep",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                ],
                                              ),
                                            ),

                                            Spacer(),
                                            TextWidget(
                                                "\$42.26",
                                                false,
                                                FontWeight.w500,
                                                1.4,
                                                Constants.pinegreen,
                                                TextAlign.start,
                                                'AvenirDemi'),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      child: Divider(
                                        thickness: 2,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Container(
                                        margin: EdgeInsets.only(top: 20),
                                        width: double.infinity,
                                        height: 70,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Image.asset(
                                                    'assets/shape.png',
                                                    height: 20,
                                                    width: 20,
                                                  )
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Constants.faddedred),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  TextWidget(
                                                      "Collection",
                                                      false,
                                                      FontWeight.w500,
                                                      1.2,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'AvenirDemi'),
                                                  TextWidget(
                                                      "Order# wov-villh",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Colors.black,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                  TextWidget(
                                                      "Date 07 Sep",
                                                      false,
                                                      FontWeight.w500,
                                                      1.1,
                                                      Constants.pinegreen,
                                                      TextAlign.start,
                                                      'pnregular'),
                                                ],
                                              ),
                                            ),

                                            Spacer(),
                                            TextWidget(
                                                "\$42.26",
                                                false,
                                                FontWeight.w500,
                                                1.4,
                                                Constants.pinegreen,
                                                TextAlign.start,
                                                'AvenirDemi'),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
