import 'package:flutter/material.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/dashboard/History.dart';

import 'package:foodamorerider/dashboard/StartShiftNow.dart';

//import 'package:food_a_more/dashboard/profile.dart';
//import 'package:food_a_more/dashboard/saved.dart';

import 'Maps.dart';
import 'Statuss.dart';
import 'StartShiftNow.dart';
import 'ShiftBooked.dart';
import 'NoPendingDeliveries.dart';
import 'NewOrder.dart';
import 'Wallet.dart';
import 'availableShift.dart';

class bottomnav extends StatefulWidget {
  var index;
  var check;

  bottomnav({this.index, this.check});

  @override
  State<bottomnav> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<bottomnav> {
  int _selectedIndex = 0;
  int getnumb = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[];

  @override
  void initState() {
    _selectedIndex = widget.index;
    super.initState();
    _widgetOptions = <Widget>[
      widget.check == 0 ? StartShift() : HomeScreen(),
     // widget.check == 4 ? newOrder() : HomeScreen(),
      deliveriesScreen(),
      Maps(),
      History(),
      Wallet(),
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child:

              _widgetOptions.elementAt(_selectedIndex),
        ),
          bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          selectedItemColor: Constants.faddedred,
          unselectedItemColor: Constants.greyishthree,
          selectedFontSize: 14,
          unselectedFontSize: 14,
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          items: [
            BottomNavigationBarItem(
              title: Text('Status'),
              icon: Image.asset(
                "assets/chargedCircle.png",
                height: 25,
              ),
            ),
            BottomNavigationBarItem(
              title: Text('Deliveries'),
              icon: Image.asset("assets/db.png"),
            ),
            BottomNavigationBarItem(
              title: Text('Map'),
              icon: Image.asset(
                "assets/nounMappp.png",
                height: 25,
              ),
            ),
            BottomNavigationBarItem(
              title: Text('History'),
              icon: Icon(Icons.history),
            ),
            BottomNavigationBarItem(
              title: Text('Wallet'),
              icon: Icon(Icons.wallet_travel),
            ),
          ],
        ));
  }
}
