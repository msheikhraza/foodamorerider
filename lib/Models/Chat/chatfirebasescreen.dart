import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';



import 'messages.dart';
import 'new_message.dart';

class ChatScreen extends StatefulWidget {
  final appointmentId;
  final seconId;

  ChatScreen(this.appointmentId, this.seconId);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {

@override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: AppBar(

          title: Container(
              padding: EdgeInsets.only(top: 5, right: 5),
              child: Text("Chat Screen", style: TextStyle(color: Colors.white),)),
          backgroundColor:Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20)),
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[   
            Expanded(
              child: Messages(widget.appointmentId, widget.seconId),
            ),
            NewMessage(widget.appointmentId,widget.seconId),

          ],
        ),

      ),
    );
  }
}
