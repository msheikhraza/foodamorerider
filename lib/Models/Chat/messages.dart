import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'message_bubble.dart';

class Messages extends StatefulWidget {
  final String appointmentId;
  final String scondid;
  Messages(this.appointmentId, this.scondid);
  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
 /* Future<String> getLawid() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('lawwwid');
  }*/

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (ctx, futureSnapshot) {
        if (futureSnapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance
                .collection('chatroom')
                .doc("${widget.appointmentId}-${widget.scondid}")
                .collection('chat')
                .orderBy(
                  'createdAt',
                  descending: true,
                )
                .snapshots(),
            builder: (ctx, chatSnapshot) {
              final chatDocs = chatSnapshot.data?.docs;
              if (chatSnapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }


              return chatSnapshot.hasData
                  ? ListView.builder(
                      reverse: true,
                      itemCount: chatDocs!.length,
                      itemBuilder: (ctx, index) => MessageBubble(
                        chatDocs[index]['message'],
                        chatDocs[index]['sendBy'] ==
                           widget.appointmentId ,
                      ),
                    )
                  : Center(
                      child: Text(
                      "No Messages",
                      style: TextStyle(color: Colors.black),
                    ));
            });
      },
      initialData: FirebaseAuth.instance.currentUser,
    );
  }
}
