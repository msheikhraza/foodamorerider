
class RideData{
  var customerId;
  var customerLatitude;
  var customerLongitude;
  var customerName;
  var orderAddress;
  var orderDetails;
  var orderId;
  var orderNote;
  var shopName;
  var status;
  var totalPrice;
  var vendorId;
  var vendorLatitude;
  var vendorLongitude;
  var vendorName;

  RideData({this.customerId , this.customerLatitude , this.customerLongitude , this.customerName , this.orderAddress ,  this.orderDetails , this.vendorId ,
             this.orderId , this.orderNote , this.shopName , this.status , this.totalPrice , this.vendorLatitude , this.vendorLongitude , this.vendorName ,  });
}