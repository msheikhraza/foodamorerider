import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/intropages/intropage.dart';
import 'package:foodamorerider/provider/authprovider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'authview/signin.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

getprefvalue();
    Timer(
        Duration(seconds: 5),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) =>prefvalue == "1"? Signin() : IntroPage()))
    );
  }
getprefvalue()async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
setState(() {
  prefvalue=   prefs.getString("key");
});





}
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: MediaQuery
                .of(context)
                .size
                .height,
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Stack(
              children: [

                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: Image.asset(
                    'assets/splashbg.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 100),
                  child: Align(
                    alignment: Alignment.center,
                    child: SvgPicture.asset(
                      'assets/logo.svg',
                      // height: 67,
                      // width: 229,

                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
