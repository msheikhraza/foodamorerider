import 'package:flutter/material.dart';

class Constants {


  static const Color introheadingcolor = Color(0xff0b2e40);
  static const TextStyle introheading=TextStyle(
    fontSize: 28,
    fontFamily: 'AvenirDemi',
    // fontFamily: 'pnbold',
    color: introheadingcolor,
  );


  static const Color introsubheadingcolor = Color(0xff7e7b7b);

  static const TextStyle introsubheading=TextStyle(
    fontSize: 16,
    fontFamily: 'AvenirNextRegular',
    // fontFamily: 'pnbold',
    color: introheadingcolor,
  );

  static const Color softPink = Color(0xffffafb9);
  static const Color rosyPink = Color(0xfff05468);
  static const Color greyy = Color(0xff7e7b7b);
  static const Color faddedred = Color(0xffdc3b50);
  static const Color whitetwelve = Color(0xffd9d9d9);
  static const Color whitegrey = Color(0xff0b2e40);
  static const Color whitish = Color(0xffababab);
  static const Color pigpink = Color(0xfff4bdc4);
  static const Color whitethirteen = Color(0xfff0eeee);
  static const Color purplebrown = Color(0xff2c2627);
  static const Color greyishthree = Color(0xffaba5a5);
  static const Color pinkishgrey = Color(0xffc4c3c3);
  static const Color pinegreen = Color(0xff0b2e40);
  static const Color duskyblue = Color(0xff4460a0);
  static const Color darkblue = Color(0xff161545);
  static const Color darkindigo = Color(0xff0c0b52);
  static const Color warmgrey = Color(0xff7e7b7b);
  static const Color warmblue = Color(0xff0b2e40);
  static const Color whitesixteen = Color(0xffededed);


  static const Color butterscotch = Color(0xfffabc41);
  static const Color cobalt_two = Color(0xff244495);
  static const Color whitetwo = Color(0xffffffff);
  static const Color whitetFourteen = Color(0xfff0eeee);
  static const Color coolBlue = Color(0xff5270bd);
  static const Color rosepink = Color(0xffef878c);
  static const Color whiteFifteen  = Color(0xfff5f5f5);
  static const Color pinkishGrey  = Color(0xffc4c3c3);


}