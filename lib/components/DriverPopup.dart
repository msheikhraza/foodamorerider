import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

class CustomDialogBox extends StatefulWidget {
  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  var _Select = false;
  var _Select2 = false;
  var _Select3 = false;
  var _Select4 = false;
  var _Select5 = false;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return SingleChildScrollView(
      child: Container(
        // color: Color.fromARGB(51, 51, 51,2),
        height: MediaQuery.of(context).size.height / 1.1,
        width: MediaQuery.of(context).size.width / 1.2,
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 1.5,
              width: MediaQuery.of(context).size.width / 1.2,
              margin: EdgeInsets.only(top: 70),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Constants.whitetwo,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      child: TextWidget("Rate Driver", false, FontWeight.w500,
                          1.5, Colors.black, TextAlign.center, 'AvenirDemi'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          "assets/reviewprofile.png",
                          height: 70,
                          width: 70,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextWidget("Jhone", false, FontWeight.w500,
                          1.5, Colors.black, TextAlign.center, 'AvenirDemi'),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: RatingBar.builder(
                        initialRating: 0,
                        minRating: 0,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkWell(
                            onTap: () {
                              setState(() {
                                _Select = true;
                              });
                            },
                            child: _Select == true
                                ? Container(
                                    height: 30,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        color: Constants.faddedred,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Punctuality",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.whitetwo,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  )
                                : Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.whiteFifteen,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Punctuality",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.pinegreen,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _Select2 = true;
                              });
                            },
                            child: _Select2 == true
                                ? Container(
                                    height: 30,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        color: Constants.faddedred,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Good Service",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.whitetwo,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  )
                                : Container(
                                    height: 30,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        color: Constants.whiteFifteen,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Good Service",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.pinegreen,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _Select3 = true;
                              });
                            },
                            child: _Select3 == true
                                ? Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.faddedred,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Clean",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.whitetwo,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  )
                                : Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.whiteFifteen,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Clean",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.pinegreen,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              setState(() {
                                _Select4 = true;
                              });
                            },
                            child: _Select4 == true
                                ? Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.faddedred,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Careful",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.whitetwo,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  )
                                : Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.whiteFifteen,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Careful",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.pinegreen,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _Select5 = true;
                              });
                            },
                            child: _Select5 == true
                                ? Container(
                                    height: 30,
                                    width: 100,
                                    decoration: BoxDecoration(
                                        color: Constants.faddedred,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Work hard",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.whitetwo,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  )
                                : Container(
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                        color: Constants.whiteFifteen,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Center(
                                      child: TextWidget(
                                          "Work hard",
                                          false,
                                          FontWeight.w300,
                                          1,
                                          Constants.pinegreen,
                                          TextAlign.center,
                                          'AvenirNextRegular'),
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Container(
                        height: 70,
                        width: 271,
                        decoration: BoxDecoration(
                            color: Constants.whitetwo,
                            borderRadius: BorderRadius.circular(4),
                            border: Border.all(color: Constants.pinkishGrey)),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, ),
                          child: TextField(
                            maxLines: 10,
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Leave a review (maximum 50 words)',
                              hintStyle: TextStyle(
                                  color: Constants.pinkishGrey,
                                  fontFamily: 'AvenirNextRegular',
                                  fontSize: 13,
                                  letterSpacing: 1.1),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                              height: 32,
                              width: MediaQuery.of(context).size.width/3,
                              decoration: BoxDecoration(
                                color: Constants.whitetwo,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Center(
                                  child: TextWidget(
                                      "Cancel",
                                      false,
                                      FontWeight.w400,
                                      1.1,
                                      Constants.pinegreen,
                                      TextAlign.center,
                                      "AvenirNextRegular")),
                            ),
                          ),

                          InkWell(
                            onTap: (){
                              //
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (BuildContext context) =>
                              //         SearchFilter()));
                            },
                            child: Container(
                              height: 32,
                              width: MediaQuery.of(context).size.width/3,
                              decoration: BoxDecoration(
                                color: Constants.faddedred,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Center(
                                  child: TextWidget(
                                      "Next",
                                      false,
                                      FontWeight.w500,
                                      1.2,
                                      Colors.white,
                                      TextAlign.center,
                                      "AvenirDemi")),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
