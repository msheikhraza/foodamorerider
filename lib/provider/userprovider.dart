import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class UserProvider extends ChangeNotifier{
  String? imageGet;
  String? name;
  void getRiderInfo() async{
final user = FirebaseAuth.instance.currentUser!.uid;
print(user);
  await  FirebaseFirestore.instance.collection("Rider").doc(FirebaseAuth.instance.currentUser!.uid).get().then((value) {
      imageGet = value.data()!['Image'];
      name = value.data()!['name'];
      notifyListeners();
    });
  }
}