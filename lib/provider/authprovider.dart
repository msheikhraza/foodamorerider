import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodamorerider/Global/global.dart';
import 'package:foodamorerider/authview/signin.dart';
import 'package:foodamorerider/widgets/appnavigaton.dart';
import 'package:foodamorerider/dashboard/Statuss.dart';
import 'package:foodamorerider/dashboard/bottom_nav.dart';
import 'package:foodamorerider/widgets/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  // final firebaseUser= FirebaseAuth.instance.currentUser!.uid;
  bool isLoading = false;
  bool forgetpass = false;

  void signInMethod(String email, String password, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isLoading = true;
    notifyListeners();

    try {
      UserCredential users = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      if (users.user!.uid != null) {
        
        prefs.setString("key", "1");
      
        AppNavigation.navigateTo(context, bottomnav(index: 0));
        isLoading = false;

        notifyListeners();
      }
    } on FirebaseAuthException catch (e) {
      print(e.toString() + "Sdsdsd");
      if (e.code == 'user-not-found') {
        Toast.toastMessage("No user found for that email.");
        isLoading = false;
        notifyListeners();
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        Toast.toastMessage("Wrong password provided for that user.");
        isLoading = false;
        notifyListeners();
      } else if (e.code ==
          'There is no user record corresponding to this identifier') {
        Toast.toastMessage("No user found");
        isLoading = false;
        notifyListeners();
      }
    }

    // FirebaseFirestore.instance.collection("Rider").doc(FirebaseAuth.instance.currentUser!.uid).get().then((value) {
    //   value.forEach((element) {
    //     if (element.data()['email'] == email &&
    //         element.data()['password'] == password &&
    //         element.data()['status'] == true) {
    //
    //       AppNavigation.navigateTo(context, bottomnav(index: 0));
    //
    //       isLoading = false;
    //       notifyListeners();
    //
    //     } else {
    //
    //     }
    //   });
    // });
  }

  void forgetPassword(BuildContext context, String email) async {
    forgetpass = true;
    notifyListeners();

    FirebaseAuth.instance.sendPasswordResetEmail(email: email).then((value) {
      Toast.toastMessage("Recovery Email Send Success");
      AppNavigation.navigateTo(context, Signin());
      forgetpass = false;
      notifyListeners();
    }).onError((error, stackTrace) {
      forgetpass = false;
      notifyListeners();
      Toast.toastMessage(error.toString());
    });
  }
}
