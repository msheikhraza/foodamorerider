import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodamorerider/authview/signin.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';


class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  PageController controller = PageController();
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[
            PageView(
              onPageChanged: (value) {
                setState(() {
                  pageIndex = value;
                });
              },
              controller: controller,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
//                decoration: BoxDecoration(
//                    image: DecorationImage(image: AssetImage('assets/logo.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 2.8,
                        child: SvgPicture.asset(
                          'assets/introimg1.svg',
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 1.8,
                                child: TextWidget(
                                    "Delivery Food to your door",
                                    false,
                                    FontWeight.w800,
                                    1.8,
                                    Constants.pinegreen,
                                    TextAlign.center,
                                    'AvenirDemi'),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 32, vertical: 16.0),
                                child:
                                TextWidget(
                                    "Find the right restaurant for you",
                                    false,
                                    FontWeight.w500,
                                    1.2,
                                    Constants.warmgrey,
                                    TextAlign.center,
                                    'AvenirNextRegular'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
//                decoration: BoxDecoration(
//                    image: DecorationImage(image: AssetImage('assets/logo.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 2.8,
                        child: SvgPicture.asset(
                          'assets/introimg2.svg',
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 1.5,
                                child: Text("Find all restaurant in one app",
                                    textAlign: TextAlign.center,
                                    style: Constants.introheading),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 32, vertical: 16.0),
                                child: Text(
                                  "Find the right restaurant for you",
                                  textAlign: TextAlign.center,
                                  style: Constants.introsubheading,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
//                decoration: BoxDecoration(
//                    image: DecorationImage(image: AssetImage('assets/logo.png'))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 2.8,
                        child: SvgPicture.asset(
                          'assets/introimg1.svg',
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width / 1.8,
                                child: Text("Delivery Food to your door",
                                    textAlign: TextAlign.center,
                                    style: Constants.introheading),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 32, vertical: 16.0),
                                child: Text(
                                  "Find the right restaurant for you",
                                  textAlign: TextAlign.center,
                                  style: Constants.introsubheading,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              top: MediaQuery.of(context).size.height / 1.7,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(2.0),
                            height: pageIndex == 0 ? 10 : 5,
                            width: pageIndex == 0 ? 10 : 5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                shape: BoxShape.rectangle,
                                color: pageIndex == 0
                                    ? Constants.faddedred
                                    : Constants.whitetwelve),
                          ),
                          Container(
                            margin: EdgeInsets.all(2.0),
                            height: pageIndex == 1 ? 10 : 5,
                            width: pageIndex == 1 ? 10 : 5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                shape: BoxShape.rectangle,
                                color: pageIndex == 1
                                    ? Constants.faddedred
                                    : Constants.whitetwelve),
                          ),
                          Container(
                            margin: EdgeInsets.all(2.0),
                            height: pageIndex == 2 ? 10 : 5,
                            width: pageIndex == 2 ? 10 : 5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                shape: BoxShape.rectangle,
                                color: pageIndex == 2
                                    ? Constants.faddedred
                                    : Constants.whitetwelve),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: [
                  Spacer(),
                  InkWell(
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Constants.faddedred,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        height: 48,
                        width: 300,
                        child: Center(
                          child: Text(
                            "Sign In",
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontFamily: "AvenirDemi",
                                letterSpacing: 0.18,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => Signin()));
                    },
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width / 1.1,
                      child: Center(
                        child: InkWell(
                          onTap: (){
                                       Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (BuildContext context) => Signin()));
                          },
                          child: Text(
                            "Skip",
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: "AvenirDemi",
                                color: Constants.faddedred,
                                letterSpacing: 0.18,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
