import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/dashboard/bottom_nav.dart';
import 'package:foodamorerider/provider/authprovider.dart';
import 'package:foodamorerider/widgets/appnavigaton.dart';
import 'package:provider/provider.dart';


import 'forget_pas.dart';

class Signin extends StatefulWidget {
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  bool _obscureText = true;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {


    return SafeArea(

      child: SafeArea(
        child: Scaffold(

          resizeToAvoidBottomInset: false,
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [

                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Form(
                  key:  _formKey,
                    child: Consumer<AuthProvider>(
                     builder: (context, auth, _){
                       return
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             SizedBox(
                               height: 40,
                             ),
                             SvgPicture.asset(
                               "assets/signinlogo.svg",
                               height: 78,
                               width: MediaQuery.of(context).size.width / 2,
                             ),
                             SizedBox(
                               height: 20,
                             ),
                             TextWidget(
                                 "Welcome Back",
                                 false,
                                 FontWeight.normal,
                                 2.2,
                                 Constants.introheadingcolor,
                                 TextAlign.center,
                                 'AvenirNextBold'),
                             SizedBox(
                               height: 10,
                             ),
                             TextWidget(
                                 "Sign in to continue",
                                 false,
                                 FontWeight.normal,
                                 1.250,
                                 Constants.purplebrown,
                                 TextAlign.center,
                                 'AvenirNextRegular'),
                             Container(
                               width: MediaQuery.of(context).size.width / 1.2,
                               child: new TextFormField(
                                 controller: emailController ,
                                 decoration: new InputDecoration(
                                   labelText: "Rider ID",
                                   labelStyle: TextStyle(
                                       color: Constants.greyishthree,
                                       fontSize: 16,
                                       fontFamily: 'AvenirNextRegular'),
                                   enabledBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(
                                       color: Constants.greyishthree,
                                     ),
                                   ),
                                   focusedBorder: UnderlineInputBorder(
                                     borderSide: const BorderSide(
                                         color: Constants.greyishthree, width: 2.0),
                                   ),
                                   fillColor: Colors.white,
                                 ),
                                 keyboardType: TextInputType.emailAddress,
                                 style: new TextStyle(
                                     color: Constants.purplebrown,
                                     fontFamily: 'AvenirNextRegular',
                                     fontSize: 17),
                                 validator: (val) {
                                   if(val!.length==0) {
                                     return "Email cannot be empty";
                                   }else{
                                     return null;
                                   }
                                 },

                               ),
                             ),
                             SizedBox(
                               height: 10,
                             ),
                             Container(
                               width: MediaQuery.of(context).size.width / 1.2,
                               child: new TextFormField(
                                 autocorrect: true,
                                 controller: passwordController,
                                 decoration: new InputDecoration(
                                   suffixIcon: InkWell(
                                     child: Image.asset("assets/eye.png"),
                                     onTap: () {
                                       _toggle();
                                     },
                                   ),
                                   labelText: "Password",
                                   labelStyle: TextStyle(
                                       color: Constants.greyishthree,
                                       fontSize: 16,
                                       fontFamily: 'AvenirNextRegular'),
                                   enabledBorder: UnderlineInputBorder(
                                     borderSide: BorderSide(
                                       color: Constants.greyishthree,
                                     ),
                                   ),
                                   focusedBorder: UnderlineInputBorder(
                                     borderSide: const BorderSide(
                                         color: Constants.greyishthree, width: 2.0),
                                   ),
                                   fillColor: Colors.white,
                                 ),
                                 validator: (val) {
                                   if(val!.length==0) {
                                     return "password cannot be empty";
                                   }else{
                                     return null;
                                   }
                                 },
                                 keyboardType: TextInputType.visiblePassword,
                                 style: new TextStyle(
                                     color: Constants.purplebrown,
                                     fontFamily: 'AvenirNextRegular',
                                     fontSize: 17),
                                 obscureText: _obscureText,
                               ),
                             ),
                             SizedBox(
                               height: 20,
                             ),
                             Padding(
                               padding: const EdgeInsets.only(right: 30),
                               child: InkWell(
                                 child: Align(
                                   alignment: Alignment.centerRight,
                                   child: TextWidget(
                                       "Forgot Password?",
                                       false,
                                       FontWeight.normal,
                                       1.0,
                                       Constants.pinegreen,
                                       TextAlign.center,
                                       'AvenirNextRegular'),
                                 ),
                                 onTap: () {
                                   Navigator.of(context).push(MaterialPageRoute(
                                       builder: (BuildContext context) =>
                                           Forgetpass()));
                                 },
                               ),
                             ),
                             SizedBox(
                               height: 10,
                             ),

                             auth.isLoading? CircularProgressIndicator(color: Colors.red,): Container(),
                             SizedBox(
                               height: 10,
                             ),
                             InkWell(
                               child: Container(
                                 width: MediaQuery.of(context).size.width / 1.2,
                                 height: 48,
                                 child: Center(
                                   child: TextWidget(
                                       "Sign In",
                                       false,
                                       FontWeight.w500,
                                       1.2,
                                       Colors.white,
                                       TextAlign.center,
                                       'AvenirDemi'),
                                 ),
                                 decoration: BoxDecoration(
                                     borderRadius: BorderRadius.circular(11),
                                     color: Constants.faddedred),
                               ),
                               onTap: () {

                                 if(_formKey.currentState!.validate()){


                                   auth.signInMethod(emailController.text , passwordController.text, context);



                                 }



                               },
                             ),
                             SizedBox(
                               height: 20,
                             ),
                             Stack(

                               children: [
                                 Container(
                                   child: Column(
                                     children: [

                                       Container(
                                         height: 140,
                                         width: 400,
                                         child: Container(


                                           width:
                                           MediaQuery.of(context).size.width / 1.1,

                                           child: Padding(
                                             padding: const EdgeInsets.only(top: 10),
                                             child: Column(
                                               children: [
                                                 Row(
                                                   children: [
                                                     Padding(
                                                       padding: const EdgeInsets.only(left: 65),
                                                       child: TextWidget(
                                                           "By logging in you agree to FoodaMore",
                                                           false,
                                                           FontWeight.w500,
                                                           1.0,
                                                           Constants.pinegreen,
                                                           TextAlign.center,
                                                           'AvenirDemi'),
                                                     ),


                                                   ],
                                                 ),

                                                 TextWidget(
                                                     "Terms and services, Privacy Policy & Content policy",
                                                     false,
                                                     FontWeight.w500,
                                                     1.0,
                                                     Constants.faddedred,
                                                     TextAlign.start,
                                                     'AvenirDemi'),



                                               ],
                                             ),
                                           ),
                                           decoration: BoxDecoration(
                                             image:  DecorationImage(
                                               image: AssetImage(
                                                   'assets/designClipart.png'),
                                               fit: BoxFit.fill,
                                             ),
                                           ),
                                         ),
                                       ),
                                     ],
                                   ),
                                 ),
                               ],
                             )
                           ],
                         );
                     },

                         ),





                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
