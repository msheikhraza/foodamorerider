import 'package:flutter/material.dart';
import 'package:foodamorerider/authview/signin.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';

//import 'package:food_a_more/authview/verfication.dart';


class Sign_up extends StatefulWidget {


  @override
  _Sign_upState createState() => _Sign_upState();
}

class _Sign_upState extends State<Sign_up> {
  var checkBoxValue=false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(

          child: Column(
            children: [
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child: InkWell(
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child:Icon(Icons.arrow_back_ios,
                        color:    Constants.introheadingcolor,
                      )
                  ),
                  onTap: (){
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(height: 10,),
              TextWidget(
                  "Signup",
                  false,
                  FontWeight.normal,
                  2.2,
                  Constants.introheadingcolor,
                  TextAlign.center,
                  'AvenirDemi'),
              SizedBox(height: 10,),
              TextWidget(
                  "It’s great to see you",
                  false,
                  FontWeight.normal,
                  1.250,
                  Constants.purplebrown,
                  TextAlign.center,
                  'AvenirNextRegular'),
              SizedBox(height: 10,),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width/1.2,
                child: new TextFormField(

                  decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.person_outline,
                      color: Colors.black,
                    ),

                    labelText: "Full Name",
                    labelStyle: TextStyle(
                        color: Constants.greyishthree,
                        fontSize: 16,
                        fontFamily: 'AvenirNextRegular'
                    ),


                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Constants.greyishthree,
                      ),
                    ),
                    focusedBorder:UnderlineInputBorder(
                      borderSide: const BorderSide(
                          color: Constants.greyishthree,
                          width: 2.0),

                    ),
                    fillColor: Colors.white,


                  ),

                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                      color: Constants.purplebrown,
                      fontFamily: 'AvenirNextRegular',
                      fontSize: 17
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width/1.2,
                child: new TextFormField(


                  decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.email_outlined,
                      color: Colors.black,
                    ),


                    labelText: "Email Address",
                    labelStyle: TextStyle(
                        color: Constants.greyishthree,
                        fontSize: 16,
                        fontFamily: 'AvenirNextRegular'
                    ),


                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Constants.greyishthree,
                      ),
                    ),
                    focusedBorder:UnderlineInputBorder(
                      borderSide: const BorderSide(
                          color: Constants.greyishthree,
                          width: 2.0),

                    ),
                    fillColor: Colors.white,


                  ),

                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                      color: Constants.purplebrown,
                      fontFamily: 'AvenirNextRegular',
                      fontSize: 17
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width/1.2,
                child: new TextFormField(


                  decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.phone_android,
                      color: Colors.black,
                    ),


                    labelText: "Phone Number",
                    labelStyle: TextStyle(
                        color: Constants.greyishthree,
                        fontSize: 16,
                        fontFamily: 'AvenirNextRegular'
                    ),


                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Constants.greyishthree,
                      ),
                    ),
                    focusedBorder:UnderlineInputBorder(
                      borderSide: const BorderSide(
                          color: Constants.greyishthree,
                          width: 2.0),

                    ),
                    fillColor: Colors.white,


                  ),
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                      color: Constants.purplebrown,
                      fontFamily: 'AvenirNextRegular',
                      fontSize: 17
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width/1.2,
                child: new TextFormField(


                  decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.lock_outline,
                      color: Colors.black,
                    ),
                    labelText: "Password",
                    labelStyle: TextStyle(
                        color: Constants.greyishthree,
                        fontSize: 16,
                        fontFamily: 'AvenirNextRegular'
                    ),


                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Constants.greyishthree,
                      ),
                    ),
                    focusedBorder:UnderlineInputBorder(
                      borderSide: const BorderSide(
                          color: Constants.greyishthree,
                          width: 2.0),

                    ),
                    fillColor: Colors.white,


                  ),

                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                      color: Constants.purplebrown,
                      fontFamily: 'AvenirNextRegular',
                      fontSize: 17
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width/1.2,
                child: new TextFormField(


                  decoration: new InputDecoration(
                    prefixIcon: Icon(
                      Icons.lock_outline,
                      color: Colors.black,
                    ),


                    labelText: "Retype Password",
                    labelStyle: TextStyle(
                        color: Constants.greyishthree,
                        fontSize: 16,
                        fontFamily: 'AvenirNextRegular'
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Constants.greyishthree,
                      ),
                    ),
                    focusedBorder:UnderlineInputBorder(
                      borderSide: const BorderSide(
                          color: Constants.greyishthree,
                          width: 2.0),

                    ),
                    fillColor: Colors.white,


                  ),

                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(
                      color: Constants.purplebrown,
                      fontFamily: 'AvenirNextRegular',
                      fontSize: 17
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Transform.scale(
                    scale: 1.2,
                    child: Checkbox(
                      value: checkBoxValue,
                      activeColor:    Constants.faddedred,
                      // border: BorderSide(color: Colors.white),
                      focusColor: Colors.pink,
                      onChanged: (bool? value) {
                        setState(() {
                          checkBoxValue = value!;
                        });
                      },

                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/1.4,
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(
                              text: 'I agree to ',
                              style: new TextStyle(
                                  fontSize: 12,
                                  color: Constants.purplebrown,
                                  fontFamily: 'AvenirNextRegular',
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.5

                              )
                          ),
                          TextSpan(
                              text: 'Foodamore ',
                              style: new TextStyle(
                                  fontSize: 12,
                                  color: Constants.purplebrown,
                                  fontFamily: 'AvenirNextRegular',
                                  fontWeight: FontWeight.w800,
                                  letterSpacing: 0.5

                              )
                          ),
                          new TextSpan(text: 'Terms and services, Privacy policy,',
                              style: new TextStyle(
                                  fontSize: 12,
                                  color: Constants.faddedred,
                                  fontFamily: 'AvenirNextRegular',
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.5

                              )
                          ),
                          new TextSpan(
                              text: 'and Content policy',
                              style: new TextStyle(
                                  fontSize: 12,
                                  color: Constants.purplebrown,
                                  fontFamily: 'AvenirNextRegular',
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.5

                              )
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 20,),
              InkWell(
                child: Container(
                  width: MediaQuery.of(context).size.width/1.2,
                  height: 48,
                  child: Center(
                    child:   TextWidget(
                        "Sign up",
                        false,
                        FontWeight.w500 ,
                        1.2,
                        Colors.white,
                        TextAlign.center,
                        'AvenirDemi'),
                  ),

                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      color:  Constants.faddedred

                  ),

                ),

              ),
              SizedBox(height: 15,),
              InkWell(
                child: RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Already have an account? ',
                          style: new TextStyle(
                              fontSize: 15,
                              color: Constants.introheadingcolor,
                              fontFamily: 'AvenirNextRegular',
                              fontWeight: FontWeight.w500,
                              letterSpacing: 0.5

                          )
                      ),
                      new TextSpan(text: 'Sign In',
                          style: new TextStyle(
                              fontSize: 15,
                              color: Constants.faddedred,
                              fontFamily: 'AvenirNextRegular',
                              fontWeight: FontWeight.w500,
                              letterSpacing: 0.5

                          )
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Signin()));
                },
              ),
            ],
          ),

        ),


      ),
    );
  }
}
