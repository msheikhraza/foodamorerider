import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:foodamorerider/authview/signin.dart';
import 'package:foodamorerider/components/const.dart';
import 'package:foodamorerider/components/textwidget.dart';
import 'package:foodamorerider/provider/authprovider.dart';
import 'package:foodamorerider/widgets/appnavigaton.dart';
import 'package:provider/provider.dart';

class Forgetpass extends StatefulWidget {
  @override
  _ForgetpassState createState() => _ForgetpassState();
}

class _ForgetpassState extends State<Forgetpass> {
  TextEditingController forgetpasscontroller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override    
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Consumer<AuthProvider>(builder: (context, forgetModel, _) {
                return Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          children: [
                            InkWell(
                              child: Icon(
                                Icons.arrow_back,
                                size: 30,
                                color: Constants.pinegreen,
                              ),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      ),
                      TextWidget(
                          "Forgot Password",
                          false,
                          FontWeight.normal,
                          2.0,
                          Constants.introheadingcolor,
                          TextAlign.center,
                          'AvenirDemi'),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.3,
                        child: TextWidget(
                            "Please enter your email address. You will recive a link to create a new password via email.",
                            false,
                            FontWeight.normal,
                            1.2,
                            Constants.warmgrey,
                            TextAlign.center,
                            'AvenirNextRegular'),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Image.asset(
                        'assets/forgetpass.png',
                        height: MediaQuery.of(context).size.height / 3,
                        width: MediaQuery.of(context).size.width / 1.5,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: new TextFormField(
                          controller: forgetpasscontroller,
                          decoration: new InputDecoration(
                            prefixIcon: Icon(
                              Icons.email_outlined,
                              color: Constants.purplebrown,
                            ),
                            labelText: "Enter your email address",
                            labelStyle: TextStyle(
                                color: Constants.greyishthree,
                                fontSize: 16,
                                fontFamily: 'Enter your email address'),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Constants.whitetwelve,
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Constants.greyishthree, width: 2.0),
                            ),
                            fillColor: Colors.white,
                          ),
                          validator: (val) {
                            if (val!.length == 0) {
                              return "Email cannot be empty";
                            } else {
                              return null;
                            }
                          },
                          keyboardType: TextInputType.emailAddress,
                          style: new TextStyle(
                              color: Constants.purplebrown,
                              fontFamily: 'pnregular',
                              fontSize: 17),
                        ),
                      ),
                      Spacer(),
                      forgetModel.forgetpass
                          ? CircularProgressIndicator(
                              color: Colors.red,
                            )
                          : Container(),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: 48,
                          child: Center(
                            child: TextWidget(
                                "Send",
                                false,
                                FontWeight.w500,
                                1.2,
                                Colors.white,
                                TextAlign.center,
                                'AvenirDemi'),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Constants.faddedred,
                          ),
                        ),
                        onTap: () {
                          // if (_formKey.currentState!.validate()) {
                          //   forgetModel.forgetPassword(
                          //       context, forgetpasscontroller.text);
                          // }
                          AppNavigation.navigateTo(context, Signin());
                        },
                      ),
                      SizedBox(
                        height: 80,
                      ),
                    ],
                  ),
                );
              })),
        ),
      ),
    );
  }
}
